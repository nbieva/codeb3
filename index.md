---
title: Introduction et mise en place
display: Introduction et mise en place
lang: fr-FR
---

<!-- <h1>{{ $frontmatter.display }}</h1> -->

<a href="/modules-b3/inscription" class="inscriptions-button">Inscriptions</a>

<p class="lead">Avant de plonger dans la création de nos projets à proprement parler, il nous faut mettre en place les différents éléments qui les composent et un environnement de développement suffisamment flexible.</p>

<!-- <a data-fancybox title="Pixels" href="http://www.digicult.it/wp-content/uploads/2017/10/sassoon5.gif">![](http://www.digicult.it/wp-content/uploads/2017/10/sassoon5.gif)</a>
> INDEX (dettaglio), Nicolas Sassoon, 2016 (source -> http://digicult.it/news/world-pattern-interview-nicolas-sassoon/) -->

<a data-fancybox title="Pixels" href="https://cdn.booooooom.com/wp-content/uploads/2022/07/PATTERN_11.gif">![](https://cdn.booooooom.com/wp-content/uploads/2022/07/PATTERN_11.gif)</a>
> **Nicolas Sassoon** - Waterfalls | #65 (source -> https://www.booooooom.com/2022/07/29/interview-nicolas-sassoon/)

<Gallery :imggallery="gallery"></Gallery>

## Les liens du jour

+ [Le site de la librairie P5js](https://p5js.org)
+ [Processing.org](Processing.org)
+ [Netlify](https://netlify.com)
+ [Visual Studio Code](https://code.visualstudio.com/)
+ [VSCode themes](https://vscodethemes.com/) + theme par workspace
+ [15 astuces utiles pour VSCode](https://www.youtube.com/watch?v=0-EPhWwCGXs)
+ [Le langage Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
+ [Nicolas Sassoon](https://nicolassassoon.com/) + sur le [site de la Galerie Charlot (Paris)](https://www.galeriecharlot.com/fr/85/Nicolas-Sassoon)
+ [Git](https://git-scm.com/) + [page wikipedia](https://fr.wikipedia.org/wiki/Git)
+ [Github](https://github.com/)
+ [Le repo P5js](https://github.com/processing/p5.js)
+ Terminal
+ Demo Code B3

## Au programme

+ **Objectifs** / attentes
+ La particularité de ces cours et ce qu'ils nécessitent en terme d'**investissement**
+ Modes de communication
+ Retour sur les **workshops**
+ **Spécificités** de différents langages: Processing, P5js, Python, etc.
+ **Editeurs** (Atom, Brackets, Sublime, VSCode...)
+ **Architecture du projet**
+ **Articulation de JS avec HTML et CSS**. Les 3 seront complémentaires.
+ Mise en ligne sur **Netlify** (aussi Github Pages)
+ Balises **meta**
+ Notes sur les **librairies JS**

## Exemples

+ [http://www.generative-gestaltung.de/2/](http://www.generative-gestaltung.de/2/)
+ [Open Processing](https://openprocessing.org/browse/?q=brush&time=anytime&type=tags#)
+ Typo: [http://parametric.esac-cambrai.net/](http://parametric.esac-cambrai.net/) + [http://www.esac-cambrai.net/wordpress/?page_id=1003](http://www.esac-cambrai.net/wordpress/?page_id=1003)
+ [https://otherside.otherti.me/](https://otherside.otherti.me/)
+ [Sketch en background d'une page web](https://gildasp.fr/exp/P5js-fullscreen/)
+ [Programming posters](https://timrodenbroeker.de/projects/programming-posters/) + [P5studio](https://timrodenbroeker.de/projects/p5studio/) > [Lien](https://p5studio.timrodenbroeker.now.sh/)
+ [https://p5studio.timrodenbroeker.now.sh/](https://p5studio.timrodenbroeker.now.sh/)
+ [https://puckey.studio/](https://puckey.studio/) + [https://puckey.studio/projects/radio-garden](https://puckey.studio/projects/radio-garden)
+ Exemples P5js (Lauren McCarthy): [https://github.com/lmccart/gswp5.js-code](https://github.com/lmccart/gswp5.js-code)
+ [https://jspaint.app/](https://jspaint.app/)
+ **Morellet**: [https://www.youtube.com/watch?v=OrlBGi2px1Q](https://www.youtube.com/watch?v=OrlBGi2px1Q)
+ [https://www.courses.tegabrain.com/cc18/artists/](https://www.courses.tegabrain.com/cc18/artists/)
+ Capture video
+ [ml5.org](https://ml5js.org/)
+ [Teachable machine](https://teachablemachine.withgoogle.com/)

## Todos

+ Se créer un compte sur [Github](https://github.com/)
+ Créer un sketch de 800x800
+ Créer un outil de dessin avec au moins une variable aléatoire. Le mettre en ligne sur Netlify
+ Réaliser un projet pour la rentrée de février (déjà anticiper) - voir comment articuler/combiner cela avec vos travaux de CASO ou d'atelier.

## Markdown

**Markdown** : [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

<img src="/markdown.png" style="float:right;width:150px;margin:0 0 1.2rem 1.8rem;">Markdown est un langage de balisage léger créé en 2004 par John Gruber avec l'aide d'Aaron Swartz. Son but est d'offrir une **syntaxe facile à lire et à écrire**. Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé ou formaté par des instructions particulières.

Un document balisé par **Markdown peut être converti en HTML, en PDF ou en d'autres formats**.

Source: [Wikipedia](https://fr.wikipedia.org/wiki/Markdown)

En outre, il est souvent utilisé par les générateurs de sites statiques tels Jekyll, Gatsby, Hugo, Nuxt ou VuePress..

Mais c'est surtout **LE langage de la documentation de projets**. Les dépôts Git que vous visiterez sur Github, GitLab ou autres, utilisent tous Makdown.

Il est:

+ **Très simple** à prendre en main
+ **Focus sur le contenu**
+ **Exports** HTML, PDF..
+ **Extensible**

### Liens:

+ **MacDown**: [https://macdown.uranusjr.com/](https://macdown.uranusjr.com/)