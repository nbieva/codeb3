# Filiation <!-- <Badge type="tip" text="Important" /> -->

Cette page, bien entendu non-exhaustive et tout à fait subjective, reprend quelques jalons en lien avec le code. On les retrouve aussi bien dans la peinture que le design textile ou la littérature.

## Claude Chappe

<Image file="chappe_code.jpg" legend="Le télégraphe de Claude Chappe" link="https://www.google.com/search?q=t%C3%A9l%C3%A9graphe+chappe&rlz=1C5CHFA_enBE752BE753&source=lnms&tbm=isch&sa=X&ved=0ahUKEwinivWMpNbgAhXGPOwKHSCCBFkQ_AUIDigB&biw=1440&bih=766" />

## Métier à tisser Jacquard

Le design textile entretient des liens très étroits avec le monde et les logiques de la programmation, à la fois techniquement et conceptuellement.
Le métier Jacquard est souvent cité comme étant la première machine "programmable" capable de reproduire des motifs, en exécutant une série d'instructions dans un ordre précis.

+ [PROGRAMMING PATTERNS: THE STORY OF THE JACQUARD LOOM](https://www.scienceandindustrymuseum.org.uk/objects-and-stories/jacquard-loom)
+ [https://www.youtube.com/watch?v=HDROcn_0ZbI](https://www.youtube.com/watch?v=HDROcn_0ZbI)
+ [https://fr.wikipedia.org/wiki/M%C3%A9tier_Jacquard](https://fr.wikipedia.org/wiki/M%C3%A9tier_Jacquard)

<Image file="jacquard.png" legend="Le métier à tisser de Joseph-Marie Jacquard (vers 1801-1816)" link="https://fr.wikipedia.org/wiki/M%C3%A9tier_Jacquard" />
<Image file="workshop-group2.017.jpeg" legend="Les fameuses cartes perforées.." link="" />

<Image file="jacquard2.jpg" legend="Les fameuses cartes perforées (2)" link="" />

## Ada Lovelace

<Image file="ada.jpeg" legend="Ada Lovelace" link="https://www.garance-et-moi.com/blog/lespionnieres-lincroyable-histoire-dada-lovelace-pionniere-de-linformatique" />

## Sébastien Truchet

<Image file="truchet1.jpg" legend="Sébastien Truchet" link="https://fr.wikipedia.org/wiki/S%C3%A9bastien_Truchet"/>
<Image file="truchet2.jpg" legend="Sébastien Truchet" link="https://gallica.bnf.fr/ark:/12148/bpt6k15100709.image"/>


## Anni Albers

<Image 
    file="anni-albers3.jpeg" 
    legend="À gauche:  métier à tisser manuel , Mexique, env. 1930. Galerie d'art de l'université de Yale, collection commémorative Harriet Engelhardt, don de Mme Paul Moore. Droite:  Figurine féminine debout , Mexique, Guanajuato, Chupícuaro, 400–100 av. J.-C., Musée d'histoire naturelle de Yale Peabody." 
    link="https://www.artsy.net/article/artsy-editorial-josef-anni-albers-amassed-1-400-works-south-american-art" 
    textlink="Les maîtres du bauhaus Josef et Anni Albers réunis avec obsession pour l'art latino-américain"
/>

## John Cage

<Video 
    type="youtube" 
    id="gXOIkT1-QWY" 
    legend="John Cage performing Water Walk in January, 1960 on the popular TV show I've Got A Secret." 
    link="https://www.youtube.com/watch?v=gXOIkT1-QWY" 
    textlink=""
/>

<Image file="waterwalk.jpg" />

## Sol LeWitt

<Image file="certificate.jpg" link="https://llimllib.github.io/solving-sol/049/llimllib/index.html" textlink="Numéro 49"/>
<Image file="lewitt-strokes-1.png" legend="Sol LeWitt, Wavy Brushstrokes, Gouache sur papier, 1996"/>
<Image 
    file="lewitt-strokes-2.jpg" 
    legend="Sol LeWitt, Black Curvy Brushstrokes, Aquatinte, 1997" 
/>
<Image 
    file="sol_le_witt_open-cubes.jpg" 
    legend="Sol LeWitt, Open cubes" 
/>
<Video type="youtube" id="RDrHHsez3nU"/>
<Video type="youtube" id="c4cgB4vJ2XY"/>

## Manfred Mohr

<a data-fancybox title="" href="/manfred1.png">![](/manfred1.png)</a>

## Carl Andre

Carl Andre pose des mots sur le papier comme il pose des pièces de métal ou de brique sur le sol. Constituées de lettres entassées dans des blocs de mots assemblés les uns aux autres, ces poèmes, qu'il a écrits depuis les années 1960, se présentent comme des « configurations sculpturales ». Dans la tradition de la poésie concrète, les mots deviennent des entités modulaires, reconfigurables, déplacées et repositionnées dans les limites de l'espace de la feuille de papier afin de créer de nouveaux agencements et motifs, dans une intense interrogation du texte et des mots qui va de pair avec son activité sculpturale, les deux tentatives se nourrissant et se soutenant l'une l'autre très significativement.

Source: [Les Presses du Réel](http://www.lespressesdureel.com/ouvrage.php?id=3286)

<Image 
    file="carlandrepoems1.jpg" 
    legend="Carl Andre" 
    link="https://www.google.com/search?q=carl+andre+poems&source=lnms&tbm=isch&sa=X&ved=0ahUKEwipt-Lkm9TgAhVPMewKHUtSDpcQ_AUIDigB&biw=1577&bih=1289#imgdii=CioLdYvaMQa6gM:&imgrc=BFZiMr7hCenQbM:" 
    textlink="Poemes"
/>

## Raymond Queneau & l'Oulipo

« C’est somme toute une sorte de machine à fabriquer des poèmes, mais en nombre limité ; il est vrai que ce nombre, quoique limité, fournit de la lecture pour près de deux cents millions d’années (en lisant vingt-quatre heures sur vingt-quatre). […]
En comptant 45 s pour lire un sonnet et 15 s pour changer les volets, à 8 heures par jour, 200 jours par an, on a pour plus d’un million de siècles de lecture, et en lisant toute la journée 365 jours par an, pour : 190 258 751 années plus quelques plombes et broquilles (sans tenir compte des années bissextiles et autres détails). »
**Raymond Queneau**, Cent Mille Milliards de poèmes

<Video type="youtube" id="2NhFoSFNQMQ"/>
<Image file="poemes.png" />
<Image file="perec1.jpg" />
<Image file="perec3.jpg" />

**Georges Perec. Notes préparatoires pour La Vie mode d'emploi.** Bi-carré latin orthogonal d'ordre 10 réglant la répartition dans les pièces de l'immeuble (c'est-à-dire aussi dans les chapitres du livre) des éléments des listes « positions » et « activités ». Pour le chapitre lxviii, dont le « cahier des charges » figure dans cet ouvrage, c'est le couple « entrer » et « réparer » que le système a sélectionné (le petit garçon à qui l'on interdit d'entrer, l'accordeur de piano).
(Fonds privé Georges Perec déposé à la bibliothèque de l'Arsenal, 61,104)

## On Kawara

<Image file="onkawara.png" />
<Image file="card31.jpg" />
<Image file="stillalive.png" />

## Art and language

Map of itself ou Map of an area of dimension 12 ‘’ x 12 ‘’ indicating 2,304 ¼ squares, 1967, Art & Language, Collectif (Michael Baldwin & Terry Atkinson)

<Image file="mapofitself.jpg" />

## Muriel Cooper

Muriel Cooper (1925 - 26 mai 1994) était une graphiste, pionnière du design numérique, chercheuse et professeure au MIT Media Lab. Elle a travaillé pendant 6 ans au bureau des publications du MIT. Après avoir obtenu une bourse d'étude Fullbright en Italie, puis monté son propre studio de design, elle est ensuite devenue la première directrice artistique du MIT Press nouvellement créé. 

<Image 
    file="muriel-cooper.jpeg" 
    legend="Muriel Cooper" 
    link="https://fr.wikipedia.org/wiki/Muriel_Cooper" 
    textlink=""
/>

## Ikko Tanaka

<Image file="tanaka.jpg" legend="Ikko Tanaka / Nihon Buyo (1981)" 
link="https://www.moma.org/collection/works/7848"
/>

## Paul Rand

<Image file="paul_rand.jpg" legend="Works by Paul Rand"/>

## Design by numbers
DBN (Design by numbers, par John Maeda, MIT), à l'origine de Processing

<Video type="vimeo" id="72611093"/>


## Processing
Casey Reas & Ben Fry

<Image file="processing.png" legend="Processing"/>

## P5js
Lauren McCarthy

<Image file="p5js.png" legend="P5.js"/>