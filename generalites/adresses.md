---
title: Adresses utiles
lang: fr-FR
---
# Adresses utiles

## Sites de référence

+ Le site de **P5js** : [https://p5js.org/](https://p5js.org/)
+ [http://wiki.t-o-f.info/P5js/P5js](http://wiki.t-o-f.info/P5js/P5js)
+ Introduction à P5js : [https://b2renger.github.io/Introduction_p5js/](https://b2renger.github.io/Introduction_p5js/)
+ [Les pages de formation à P5js, en français, du Lycée Le Corbusier (Strasbourg)](https://www.lyceelecorbusier.eu/p5js/)
+ [Introduction to Computational Media with p5.js](https://nycdoe-cs4all.github.io/)
+ Les transformations dans P5 (Gene Kogan) : [http://genekogan.com/code/p5js-transformations/](http://genekogan.com/code/p5js-transformations/)
+ Le bruit de Perlin dans P5 (Gene Kogan) : [http://genekogan.com/code/p5js-perlin-noise/](http://genekogan.com/code/p5js-perlin-noise/)
+ P5js examples : [https://www.courses.tegabrain.com/CC17/basic-p5js-examples/](https://www.courses.tegabrain.com/CC17/basic-p5js-examples/)
+ [Introduction to Programming for the Visual Arts with p5.js](https://www.kadenze.com/courses/introduction-to-programming-for-the-visual-arts-with-p5-js/info)
+ [Programming from A to Z](https://shiffman.net/a2z/)
+ and of course, [the Coding Train](https://www.youtube.com/c/TheCodingTrain/videos)

## Divers

+ [https://scratch.mit.edu/](https://scratch.mit.edu/) : le site de Scratch (programmation par blocs)
+ [https://microbit.org/fr/](https://microbit.org/fr/) : le site du Micro:bit de la BBC
+ [https://www.raspberrypi.org/](https://www.raspberrypi.org/) : le site du nano ordinateur
+ **Paper.js**: [http://paperjs.org/examples/hit-testing/](http://paperjs.org/examples/hit-testing/)
+ [Manipuler l'information (Open classrooms)](https://openclassrooms.com/fr/courses/3930076-manipuler-linformation)
+ SVG on the web : [https://svgontheweb.com/](https://svgontheweb.com/)
+ [Les pavages de Sébastien Truchet](https://www.google.com/search?q=pavages+truchet&tbm=isch&source=univ&sa=X&ved=2ahUKEwjsoez0vqLhAhXCsKQKHSNHCRQQsAR6BAgJEAE&biw=1617&bih=978)
+ [Playing with pixels](http://playingwithpixels.gildasp.fr/) 
+ [Pour le plaisir](https://www.youtube.com/watch?v=nvH2KYYJg-o)
+ [Flow fields](https://www.bit-101.com/blog/2017/10/flow-fields-part-i/)
+ [What is a vector?](https://www.youtube.com/watch?v=bKEaK7WNLzM)

## Les bibliothèques

+ Les [bibliothèques pour étendre les possibilités de P5js](https://p5js.org/libraries/) (qui en est une également!)
+ **Vectoriel** > **[Paper.js](http://paperjs.org/examples/hit-testing/)** is an open source vector graphics scripting framework that runs on top of the HTML5 Canvas.
+ **3D** > **[Three.js](https://threejs.org/)** Discover: [https://discoverthreejs.com/book/introduction/](https://discoverthreejs.com/book/introduction/)
+ **Print** > **[Basil.js](http://basiljs.ch/about/)** est une bibliothèque javascript développée à l'[Ecole de Design de Bâle](http://thebaselschoolofdesign.ch/) pour faciliter l'utilisation de scripts personnalisés et de design génératif au coeur même d'[Adobe Indesign](https://fr.wikipedia.org/wiki/Adobe_InDesign). Il se définit comme *"An attempt to port the spirit of the Processing visualization language to Adobe Indesign."*

## Python

+ Python ex: [https://www.makeartwithpython.com/](https://www.makeartwithpython.com/)

## Showcase

+ P5js examples : [https://www.courses.tegabrain.com/CC17/basic-p5js-examples/](https://www.courses.tegabrain.com/CC17/basic-p5js-examples/)
+ Processing examples : [http://learningprocessing.com/examples/](http://learningprocessing.com/examples/)
+ Open Processing : [https://www.openprocessing.org/](https://www.openprocessing.org/)
+ Pour le plaisir : [https://www.youtube.com/watch?v=nvH2KYYJg-o](https://www.youtube.com/watch?v=nvH2KYYJg-o)
+ [Flow fields](https://www.bit-101.com/blog/2017/10/flow-fields-part-i/)