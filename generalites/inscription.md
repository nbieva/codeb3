---
title: Inscription
lang: fr-FR
---

# Inscription Code B3

Ces modules s'adressent aux étudiants de B3 inscrits dans un CASO arts numériques. Leur objectif est d'approfondir le travail avec le code rapidement évoqué en B1, afin d'enrichir vos projets dans vos options.

<!-- Les 3 premiers modules auront lieu les **3, 10 et 17 décembre de 8h45 à 10h45** (3 cours de 2h). Ces modules auront lieu **à l'auditoire Creuz**. L'auditoire n'est pas équipé de machines, il vous faudra donc apporter la vôtre ou, si vous n'en avez pas, me le faire savoir afin que je vois avec Jean-Louis comment on peut s'arranger. 

Ces trois cours reposeront les bases de ce que nous avions vu dans les workshops B1, avec Processing ou P5js.

Ces trois cours seront complétés par une journée complète de workshop, **le samedi 11 décembre de 9h à 18h**. Cette journée sera plus spécifiquement dédiée à javascript (P5js) Nous serons installés dans la salle de travail Arts numériques, à l'Abbaye.

Par la suite, nous tenterons de réorganiser l'une ou l'autre journée de workshops en janvier/février.
L'organisation des suivis de projets personnels viendra ultérieurement.

::: warning
Il est impératif de venir avec votre propre ordinateur. Si cela posait un souci, merci de nous le faire savoir via le formulaire.
:::
 -->
L'inscription se fait via le formulaire ci-dessous:

<iframe style="margin-top:30px;" src="https://docs.google.com/forms/d/e/1FAIpQLSe2UCkWcGeO4QJ9SDroi0F5vZ3ZDWECIZp6cE5cuccbEnT2yQ/viewform?embedded=true" width="740" height="968" frameborder="0" marginheight="0" marginwidth="0">Chargement…</iframe>