---
title: Les outils
lang: fr-FR
---

# Les outils pour travailler avec du code

## Les éditeurs

+ codesandbox > https://codesandbox.io/

## Les bibliothèques

+ P5js:
+ Vectoriel > **Paper.js** : http://paperjs.org/examples/hit-testing/
+ 3D > **Three.js** (https://threejs.org/, Discover: https://discoverthreejs.com/book/introduction/ , )

## Python

