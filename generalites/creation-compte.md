---
title: Mise en place p5js
lang: fr-FR
---

# Mise en place p5js

P5js est une bibliothèque javascript qui fonctionne dans un petit écosystème incluant HTML, CSS, un serveur web, etc.. La pluart d'entre vous aborderont toutes ces notions l'année prochaine dans leurs cours de web. En attendant, nous allons utiliser l'éditeur en ligne de P5js que vous trouverez à cette adresse: [https://editor.p5js.org/](https://editor.p5js.org/)

## Création de compte et sauvegardes

[Créer un compte](https://editor.p5js.org/signup) (en haut à droite) vous permettra de sauver et de partager vos sketches directement depuis l'interface de l'éditeur.

Créez autant de sketches que vous désirez durant la journée mais veillez à noter chaque URL de partage (Present) dans [cette feuille de calcul sur le Drive](https://docs.google.com/spreadsheets/d/15xHfCjfxhVV9EGBQqxm8MbZpYzrxXycIHRpP8I5qosc/edit?usp=sharing). Notez également une courte description de votre travail (ce que vous avez tenté de faire) ainsi que votre nom.

Une fois sauvé une première fois, votre sketch sera sauvé en continu automatiquement. Le raccourci **Cmd+S** forcera la sauvegarde manuellement.

<Image file="mep-save.png" legend="Sauvez votre sketch (File > save)" />

Vous pouvez également renommer votre sketch. C'est bien entendu conseillé si vous voulez vous y retrouver...

<Image file="mep-rename.png" legend="Renommez votre sketch (Donnez lui un titre)" />