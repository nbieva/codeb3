---
title: Formes simples
lang: fr-FR
---

# Formes simples

Commencez par imaginer ce que vous aimeriez réaliser.
Commencez par quelque chose de simple, utilisant un registre de formes géométriques simples.
Vous pouvez créer un drapeau, une affiche, un logo.. La taille de votre premier sketch doit être de 900px par 600px.

+ Sur le template qui vous a été remis, tracez un projet
+ **Notez les coordonnées** de vos points
+ imaginez les valeurs de couleur à entrer
+ **pensez à l'ordre dans lequel vous allez dessiner les éléments**
+ Imaginez un élément qui doive suivre votre souris
+ Certaines de vos formes peuvent ne pas avoir de contour, d'autres oui.
+ Vous pouvez également jouer sur l'opacité ou les [modes de fusion](https://p5js.org/reference/#/p5/blendMode)
+ N'hésitez pas à également utiliser des [formes libres](https://p5js.org/reference/#/p5/beginShape).
+ [Exportez votre image](/en-pratique/exporter). (Si PDF, utilisez Processing et essayez de l'ouvrir dans Illustrator pour voir ce qu'il en est. Inspectez en mode tracés)

<a data-fancybox title="ex0" href="//ex0.jpg">![ex0](//ex0.jpg)</a>

#### Notions mises en oeuvre

* noLoop() ou tout sans le Setup (pour la première partie)
* Canevas et coordonnées
* Formes simples
* Couleur (remplissage et contours)
* Eventuellement [transformations](https://genekogan.com/code/p5js-transformations/) (Ceci n'est pas évident au début..)

#### Les fonctions les plus utiles ici seront probablement :

+ fill() et noFill()
+ stroke() et noStroke()
+ strokeWeight
+ RectMode()
+ ellipseMode()
+ rect(), ellipse(), line(), point()
+ beginShape(), vertex(), endShape()
+ print()
+ save()

Utilisez le menu ci-contre, ou [la référence](https://p5js.org/reference/) P5js, pour trouver plus de détails à propos de ces fonctions.

Vous utiliserez déjà probablement les variables suivantes: width, height, frameCount, mouseX, mouseY

## Exemple

```javascript
function setup() {
    createCanvas(800,500);
    background(255);
    noLoop();
    noStroke();

    //Carrés rouges
    fill(255,0,0); // fill(rouge, vert, bleu)
    rect(0, 0, 100,100);
    rect(width-100, 0, 100,100);
    rect(width-100, height-100, 100,100);
    rect(0, height-100, 100,100);

    //Rectangles noirs
    fill(0);
    rect(200,0,400,100);
    rect(200,height-100,400,100);

    //Triangles jaunes
    fill(250,230,0);
    triangle(200, 100, 600, 100, 600, 300);
    triangle(200, height-100, 200, height-300, 600, height-100);
}
```
<a data-fancybox title="" href="/flag.png">![](/flag.png)</a>

* [Mickey](http://www.damienhirst.com/mickey), Damien Hirst

----

<a data-fancybox title="" href="/mickey.png">![](/mickey.png)</a>
