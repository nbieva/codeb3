---
title: Slit scanning
lang: fr-FR
---

# Slit scanning

[<a data-fancybox title="" href="/slit2.png">![](/slit2.png)</a>](https://www.google.be/search?q=slit+scan&rlz=1C5CHFA_enBE766BE767&source=lnms&tbm=isch&sa=X&ved=0ahUKEwimzK_gjdLZAhVlI8AKHYC7AJkQ_AUICigB&biw=1864&bih=1343)

Le slitscanning est un procédé video qui, plutôt que d'enregistrer à chaque image l'image entière, va enregistrer une tranche de cette même image (par exemple une tranche de 1px) et, plutôt que d'en afficher 30 par secondes, va les placer côte à côte dans un nouveau format.

La première image de ce type date du début du siècle dernier. Vous trouverez de nombreuses références sur ce procédé sur [cette page très complète](http://www.flong.com/archive/texts/lists/slit_scan/index.html) de Golan Levin.

Le cas échéant, prenez 10 minutes de votre temps pour suivre ce [petit tuto de Daniel Shiffman](https://www.youtube.com/watch?v=WCJM9WIoudI) sur le sujet.

Aussi cette petite expérimentation, d'[Adrien Mondot](https://www.am-cb.net/projets/cinematique), qui était venu il y a quelques années présenter son logiciel eMotion lors d'un workshop de 3 jours ici à La Cambre.
> [Le site d'adrien Mondot et Claire Bardaine](https://www.am-cb.net/)

<Video type="vimeo" id="7878518" />

Ce procédé a été utilisé par un certain nombre d'artistes, comme [Memo Akten](http://www.memo.tv/category/work/by-type/) ou [Wendy Yu](https://www.instagram.com/wendellsmindblowers/)

## Un [sketch de base](https://editor.p5js.org/WorkshopsB1/sketches/VZ9Z0XLcU)

```javascript
let video;
let x = 0;

function setup() {
  createCanvas(1920, 1080);
  video = createCapture(VIDEO);
  video.size(1920, 1080);
  video.hide();
  background(200);
}

function draw() {
  video.loadPixels();
  let w = video.width;
  let h = video.height;
  copy(video, w/2, 0, 1, h, x, 0, 1, h);
  x = x + 1;
  if (x > width) {
    x = 0;
  }
}

function keyPressed() {
    save("monSlitScan-"+day()+"-"+hour()+"-"+minute()+"-"+second()+".jpg");
}
```