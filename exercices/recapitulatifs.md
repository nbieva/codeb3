---
title: Exercices récapitulatifs
lang: fr-FR
---

<p class="lead">Quelques exercices combinant un certain nombre de fonctions.</p>

## Forme personnalisée et boucle

+ Créez une forme personnalisée.
+ Disposez-la en grille dans votre espace de travail.
+ La couleur de remplissage, de contour ainsi que l'épaisseur de contour doivent changer à chaque forme. 
+ Une fois la dernière forme dessinée, votre sketch doit être exporté au format PDF (éditable dans Illustrator) et le programme doit s'arrêter.

## Aspects de forme et exports

+ Créez un sketch qui s'affichera en plein écran, avec un fond très sombre. 
+ A chaque clic de souris dans la page, une ellipse (disque) doit apparaître à l'emplacement du clic. 
+ Elle doit être d'un diametre entre 30 et 80 pixels et d'une couleur aléatoire mais plutôt jaune, et légèrement translucide. 
+ Toutes les ellipses doivent rester visibles.
+ Sous chaque ellipse doit être écrit en rouge (en petit mais lisible) le moment où le clic a eu lieu sous la forme HH:MM:SS (par exemple: 15:33:12)
+ Si la seconde au moment du clic est entre 30 et 60, l'ellipse doit avoir un contour blanc de 15px.
+ Si l'on appuye sur la touche "e", l'image doit être sauvée au format jpg avec un nom de fichier incluant l'heure, la minute et la seconde, à la manière d'une capture d'écran. (genre "monImage-15-33-14.jpg")

<iframe class="p5embed" width="740" height="395" src="https://editor.p5js.org/nicobiev/embed/c8knJUEny"></iframe>

## Les Tableaux

+ Créez 3 tableaux différents, avec une dizaine d'entrées chacun minimum. Un pour les sujets, un pour les verbes et un pour les compléments. (Vous pouvez aussi avoir des tableaux sujets singuliers, sujets pluriels, verbes singuliers, verbes pluriels etc...)
+ A chaque clic dans la page, une phrase complète, aléatoire, doit être écrite à l'endroit du clic.
+ Seule la phrase du dernier clic doit être visible (différent de l'exemple ci-dessous, même si la matière première est semblable)

## Données

+ Idem mais avec le fichier ci-joint comme base de travail.