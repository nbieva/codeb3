---
title: Partagez votre travail
lang: fr-FR
---

# Partagez votre travail: l'éditeur de P5js

Un exercice simple pour vous familiariser avec l'interface, la syntaxe et les fonctions de base de Processing.