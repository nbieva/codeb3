---
title: 8 trames de F. Morellet
lang: fr-FR
---

# 4 doubles trames, traits minces 0°- 22°5 - 45°- 67°5

> (8 trames 0°-22°5-45°-67°5-90°-112°5-135°-157°5, Huile sur toile, 140 x 140 cm, Fait en 1958, volé en 1972, refait en 1973)

A partir des informations disponibles dans la video ci-dessous, réalisez à l'aide de Processing ou P5js (en faire une page web serait intéressant..) [le tableau de François Morellet, 8 trames](https://www.centrepompidou.fr/cpv/resource/cKag8rG/rGbxj9K).

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.3rem;">
<iframe frameborder="0" style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.3rem;" src="https://www.dailymotion.com/embed/video/xht8oh" allowfullscreen allow="autoplay"></iframe>
</div>

<iframe class="p5embed" width="740" height="740" src="https://editor.p5js.org/embed/YYvs8JTNe"></iframe>