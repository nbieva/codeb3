# socket.io

Le contenu ci-dessous est une version réduite de la documentation officielle que vous pourrez trouver sur https://socket.io/get-started/chat

Socket.io est composé de deux partie distinctes.

1) La partie serveur qui intègre socket.io et tourne avec node.js
2) La partie client composée du framework de votre choix ainsi que de la bibliothèque socket.io.js

Créons donc un dossier (celui de notre projet). A ce stade, bien évidemment, nous devons avoir déjà installé Node sur notre machine.

```javascript
npm install socket.io
```













+ creer le fichier html
+ créer styles.scss

Si SASS n'est pas encore installé sur le système
```bash
npm install -g node-sass 
```

```bash
npm init 
```

Dans l'objet scripts de package.json :

```json
"scss": "node-sass --watch assets/scss -o assets/css"
```

Lancer la commande
```bash
npm run scss //ou yarn scss
```

Maintenant que le HTML et SASS sont en places...

Installer vue
```bash
npm install -g @vue/cli
```

Ajouter sudo devant la commande si nécessaire

https://www.youtube.com/watch?v=lcYn0tgUvHE

yarn serve