---
title: Un outil de dessin
lang: fr-FR
---

# Un outil de dessin

A partir du code ci-dessous, créez un outil de dessin personnalisé (remplacez le rectangle par autre chose, travaillez la couleur..). Creusez un petit peu. Dessinez quelque chose. Exportez.

```java
var rectSize = 20; // on initialise la taille du carré

function setup() {
    // dimension du canvas
    createCanvas(800,600);
    // fond blanc
    background(255);
    rectMode(CENTER); //Les rectangles seront tracés à partir de leur centre
}

function draw(){
    // on dessine une ellipse dont les positions x et y correspondent à la position de la souris.
    rect(mouseX, mouseY, rectSize, rectSize);
}

function mouseClicked() {
    rectSize = rectSize + 20; // À chaque clic, le rectangle fait 20px de plus
}

function keyPressed() {
    save("monDessin-"+day()+"-"+hour()+"-"+minute()+"-"+second()+".jpg");
}
```

<a data-fancybox title="" href="/carres.png">![](/carres.png)</a>