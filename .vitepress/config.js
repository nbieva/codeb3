export default {
  title: 'Code B3',
  titleTemplate: 'Arts numériques',
  description: 'Support des cours B3',
  head: [
    ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js' }],
    ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js' }],
    ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css' }],
    ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,700i,900,900i&display=swap' }],
    ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css2?family=Atkinson+Hyperlegible:ital,wght@0,400;0,700;1,400;1,700&display=swap' }]
],
  markdown: {
    lineNumbers: true
  },
  
/*      configureWebpack: {
      resolve: {
        alias: {
          '@alias': 'path/to/some/dir'
        }
      }
    
  }, */
  themeConfig: {
      nav: [
        { text: 'Home', link: '/' },
          { text: 'Processing', link: 'https://processing.org' },
          { text: 'P5js', link: 'https://p5js.org' },
          /*{ text: 'Digitalab', link: 'http://digitalab.be' },*/
          { text: 'Inscriptions', link: '/generalites/inscription' }
        /* {
          text: 'Dropdown Menu',
          items: [
            { text: 'Item A', link: '/item-1' },
            { text: 'Item B', link: '/item-2' },
            { text: 'Item C', link: '/item-3' }
          ]
        } */
      ],
      sidebar: [
          {
            text: 'Modules',
            collapsible: true,
            collapsed: false,
            items: [
              { text: "Introduction", link: '/modules-b3/cours1' },
              { text: "L'élément canvas", link: '/modules-b3/cours2' },
              { text: "Les tableaux (Arrays)", link: '/modules-b3/cours3' },
              { text: "Les tableaux en pratique", link: '/modules-b3/cours4' },
              { text: "Manipuler le DOM", link: '/modules-b3/cours5' },
              { text: "Datas - le format JSON", link: '/modules-b3/cours6' },
              { text: "Images et pixels", link: '/modules-b3/onhold/images' },
              { text: "Web sockets", link: '/modules-b3/socket' },
      /*         { text: "Bases de données", link: '/modules-b3/firebase' },
              { text: "Web & Midi", link: '/modules-b3/webmidi' } */
            ]
          },
          {
            text: 'Vidéos & exercices',
            collapsible: true,
            collapsed: false,
            items: [
              { text: "Exercice - Pierre Bismuth", link: 'modules-b3/exercices/drapeau' },
              { text: "Exercice - Web clocks", link: 'modules-b3/exercices/webclock' },
              { text: "Exercices - Data", link: 'modules-b3/exercices/json' },
              { text: "https://genuary.art/", link: 'https://genuary.art/' },
              { text: "Les vidéos", link: 'modules-b3/videos' }
            ]
          }
        ]
    }
}