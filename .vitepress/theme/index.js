// .vitepress/theme/index.js
import DefaultTheme from 'vitepress/theme'
import './custom.scss'
import Video from '../components/Video.vue'
import Site from '../components/Site.vue'
import Image from '../components/Image.vue'
import ImageCours from '../components/ImageCours.vue'
import Gallery from '../components/Gallery.vue'
import cambreLayout from './cambreLayout.vue'

export default {
    ...DefaultTheme,
    Layout: cambreLayout,
    enhanceApp(ctx) {
      DefaultTheme.enhanceApp(ctx)
      ctx.app.component('Video', Video)
      ctx.app.component('Site', Site)
      ctx.app.component('Image', Image)
      ctx.app.component('ImageCours', ImageCours)
      ctx.app.component('Gallery', Gallery)
    }
  }