---
title: Les variables
lang: fr-FR
sidebarDepth: 1
---

# Les variables

L'on peut définir une variable comme un espace réservé dans la mémoire de votre ordinateur pour y stocker une valeur (d'un certain type). Cette valeur va pouvoir par la suite être utilisée, manipulée, modifiée, remplacée.
La position en X et en Y de votre souris sont deux variables. Déplacez votre souris et les valeurs de ces variables seront modifiées.

Les différentes étapes dans l'utilisation d'une variable sont les suivantes:

1. On **déclare** la variable
2. On lui **assigne** une valeur
3. On l'**utilise**
4. On la **modifie** (on parle d'incrémentation)

<a data-fancybox title="" href="/variablesp5.png">![](/variablesp5.png)</a>

On peut aussi combiner les deux premières étapes comme suit:

```javascript
var monNombre = 100;

function setup() {
    createCanvas(500,500);
}
function draw() {
    ellipse(monNombre, monNombre, monNombre/2,  monNombre/2);
    // Dessinera un cercle de 50 sur 50 positionné en 100,100
}
```

Notez que:

+ Les noms de variables ne comprennent ni espaces, ni accents, ni chiffres, et commencent par une minuscule.
+ Certains noms de variables sont "réservés", par les variables natives notamment (mouseX, frameCount, etc..)



## Incrémenter une variable

```javascript
//Déclarez les deux variables pour les positions en X et Y de votre éllipse
var posX = 0;
var posY = 150;

function setup() {
  createCanvas(750, 300);
  noStroke(); // Pas de contour
  ellipseMode(CENTER);
}

function draw() {
    background(220);
    fill(255,0,0); //rouge
    ellipse(posX, posY, 100, 100);

    // On incrémente la position en X de 1 à chaque boucle
    posX = posX + 1; // Peut aussi s'écrire posX += 1;
    if (posX == 800) {
        posX = -50;
    }
}
```

<iframe class="p5embed" height="320" src="https://editor.p5js.org/WorkshopsB1/full/8QOXzYuiB"></iframe>

## Variables natives

Il existe dans *P5js* une série de variables natives telles que **mouseX** et **mouseY**.
Si j'utilise mouseX et mouseY dans mon sketch (à l'intérieur de la boucle draw en l'occurrence..), ces variables seront remplacées par la position en X et en Y de ma souris. Ainsi pour créer un simple outil de dessin, nous pouvons utiliser le code qui suit:

```javascript
function setup() {
    createCanvas(500,500);
}
function draw() {
    ellipse(mouseX, mouseY, 5, 5);
}
```

Il existe d'autres variables natives, dont les plus importantes sont:

+ **width** : largeur de votre sketch
+ **height** : hauteur de votre sketch
+ **frameCount** : index de la boucle (sorte de compteur)
+ **pmouseX** : position précédente de la souris en X
+ **pmouseY** : position précédente de la souris en Y

Ainsi, on peut affiner notre sketch précédent comme suit:

```javascript
function setup() {
    createCanvas(750,400);
}
function draw() {
    line(pmouseX, pmouseY, mouseX, mouseY);
}
```

<iframe class="p5embed" width="740" height="350" src="https://editor.p5js.org/WorkshopsB1/full/TwoECWU4O"></iframe>

On peut aussi lier une valeur (ici le diamètre du disque) à une autre variable (ici la position de la souris en Y) :

<iframe class="p5embed" width="740" height="350" src="https://editor.p5js.org/WorkshopsB1/full/mqNCY8YGu"></iframe>


Créez un sketch dans lequel varient un grand nombre de variables simultanément (background, hauteurs, largeurs, contours, couleurs, etc.)


## Des exemples de types de variables…

>Source: [Floss manuals](https://fr.flossmanuals.net/processing/les-variables/)

Une variable est une donnée que l'ordinateur va stocker dans l'espace de sa mémoire. C'est comme un compartiment dont la taille n'est adéquate que pour un seul type d'information. Elle est caractérisée par un nom qui nous permettra d'y accéder facilement.

<a data-fancybox title="illustration_variables_memoire_1.png" href="https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-illustration_variables_memoire_1-fr-old.png">![illustration_variables_memoire_1.png](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-illustration_variables_memoire_1-fr-old.png)</a>

Le nom d'une variable peut contenir des lettres, des chiffres et certains caractères comme la barre de soulignement. À chaque fois que le programme rencontre le nom de cette variable, il peut lire ou écrire dans ce compartiment. Les variables qui vont suivre vous donneront des exemples simples de leur utilisation. Pour résumer, une variable aura un type, un nom et une valeur qui peut être lue ou modifiée.

### …avec des chiffres

Ici on décrit une variable appelée x correspondant à un chiffre décimal, puis une autre variable appelée y correspondant à une nombre entier. On appelle ensuite ces variables pour les afficher dans notre console.

```javascript
var x = 3.14159;
var y = 10;

print(x, y);
```
dans la console s'affiche :
```javascript
  3.14159 10
```
### …vraies ou fausses

On peut également créer des variables qui ne répondent qu'à deux états: *vrai* (true) ou *faux* (false). On l'utilise dans les conditions pour déterminer si une expression est vraie ou fausse. On appelle ces variables, des variables booléennes.

```javascript
var vraifaux = true;

print(vraifaux);
```
dans la console s'affiche :
```javascript
  true
```
### …avec du texte

Une variable peut également correspondre à du texte comme ci-dessous.
```javascript
var texte = "Bonjour!";

print(texte);
```
dans la console s'affiche :
```javascript
  Bonjour!
```
### …avec des couleurs

Une variable peut également correspondre à une couleur.

<a data-fancybox class="smallimg" title="damier.tiff" href="">![damier.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-damier-tiff-fr-old.png)</a>



```javascript
noStroke();
var blanc = color(255, 255, 255);
var noir = color(0, 0, 0);

fill(blanc); rect(0, 0, 25, 25);
fill(noir); rect(25, 0, 25, 25);
fill(blanc); rect(50, 0, 25, 25);
fill(noir); rect(75, 0, 25, 25);

fill(noir); rect(0, 25, 25, 25);
fill(blanc); rect(25, 25, 25, 25);
fill(noir); rect(50, 25, 25, 25);
fill(blanc); rect(75, 25, 25, 25);

fill(blanc); rect(0, 50, 25, 25);
fill(noir); rect(25, 50, 50, 25);
fill(blanc); rect(50, 50, 25, 25);
fill(noir); rect(75, 50, 25, 25);

fill(noir); rect(0, 75, 25, 25);
fill(blanc); rect(25, 75, 25, 25);
fill(noir); rect(50, 75, 25, 25);
fill(blanc); rect(75, 75, 25, 25);
```

### En savoir plus :

+ [https://p5js.org/examples/data-variables.html](https://p5js.org/examples/data-variables.html)
