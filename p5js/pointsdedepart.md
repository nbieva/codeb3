---
title: Points de départ
lang: fr-FR
---

# Exemples

+ [https://openprocessing.org/](https://openprocessing.org/)
+ [http://paperjs.org/examples/candy-crash/](http://paperjs.org/examples/candy-crash/)
+ [https://threejs.org/examples/](https://threejs.org/examples/)

# Points de départ

Vous trouverez ci-dessous une liste de points de départ, de sketches dont vous pouvez modifier les variables ou autres boucles pour vous les approprier..

Avec un peu de code, vous pouvez :

+ Dessiner un forme
+ Dessiner plusieurs formes
+ Animer une forme
+ Suivre votre souris
+ Créer un grand nombre d'éléments
+ Les faire rebondir sur les bords
+ Dessiner une image
+ Dessiner avec une image
+ Travailler evec du texte
+ Dessiner avec du texte
+ Interagir avec votre souris
+ Interagir avec votre clavier
+ Jouer un son
+ Imaginez tout ce que vous pouvez..

<!-- <a data-fancybox title="" href="/damier.png">![](/damier.png)</a>

## Code commenté

 -->