---
title: Les boucles
lang: fr-FR
sidebarDepth: 1
---

# Les répétitions, ou boucles

>Source: [Floss manuals](https://fr.flossmanuals.net/processing/les-repetitions/)

Les répétitions, ou boucles, permettent d'exécuter une série d'instructions plusieurs fois de suite. Elles évitent de dupliquer inutilement des portions de code. Attention ! les répétitions ne permettent pas de créer des animations dans le temps (d'autres instructions existent pour cela) ! Lorsque l'ordinateur lit le programme et rencontre une boucle, il va exécuter instantanément autant de fois de suite le code écrit dans le bloc de la boucle qu'on lui a indiqué.

<a data-fancybox title="Boucles" href="/for.jpg">![Boucles](/for.jpg)</a>

L'exemple ci-dessous va nous permettre d'illustrer simplement cette notion. L'objectif est de réaliser un dessin affichant dix lignes noires horizontales. Le premier code contient dix fois l'instruction line(), le second code est réalisé à partir d'une boucle. Le résultat des deux codes est le même, la différence se situant au niveau de la longueur du code, l'un étant plus rapide à saisir (et ultérieurement à modifier) que l'autre.

L'idée est de repérer dans votre code les éléments qui se répètent ou qui s'incrémentent de façon régulière.

```javascript
line(0, 0, 100, 0);
line(0, 10, 100, 10);
line(0, 20, 100, 20);
line(0, 30, 100, 30);
line(0, 40, 100, 40);
line(0, 50, 100, 50);
line(0, 60, 100, 60);
line(0, 70, 100, 70);
line(0, 80, 100, 80);
line(0, 90, 100, 90);
```

...ou plus simplement:

```js
for (var i = 0; i < 100; i = i + 10) {
    line(0, i, 100, i);
}
```

## La boucle for

Ce type de boucle permet de répéter une série d'instructions un nombre de fois défini. Elle incorpore une variable qui va s'incrémenter à chaque passage dans la boucle. On utilise souvent i comme nom pour la variable interne de la boucle. 

## Les boucles en une progression simple

Utiliser les boucles va vous permettre de générer une complexité que vous ne pourriez pas gérer manuellement. Les quelques exemples ci-dessous montrent une progression simple avec un simple dessin de lignes:

+ [Dix lignes, "manuellement"](https://editor.p5js.org/WorkshopsB1/sketches/gVTxYyVMJ)
+ [Dix lignes en utilisant une boucle for()](https://editor.p5js.org/WorkshopsB1/sketches/0_b_uF-fn)
+ [Le même sketch, mais avec les lignes jusqu'en bas de la fenêtre, , avec des épaisseurs et opacités différentes pour chaque ligne](https://editor.p5js.org/WorkshopsB1/sketches/lG-Exn-Uh)
+ Et enfin [le sketch précédant mais où l'ensemble des lignes est redessiné à chaque boucle draw()](https://editor.p5js.org/WorkshopsB1/sketches/ZO9HXI1-l)
+ Etc.

Vous pouvez augmenter encore ceci en créant un [ensemble de lignes verticales](https://editor.p5js.org/WorkshopsB1/sketches/uiFpUSq3J), en modifiant les couleurs pour chaque ligne, etc.

## Imbriquer des boucles

Les boucles peuvent s'imbriquer les une dans les autres. Cette technique permet de rapidement passer à des visualisations à deux, voir trois dimensions. Lorsqu'on imbrique des boucles, il faut prendre garde au nom que l'on donne à la variable de chaque boucle. En effet si elles se nomment toutes i, le programme va mélanger les boucles. Chaque variable de chaque boucle doit avoir un nom propre. Par exemple : i, j, k, etc. ou si elles sont liées à des dimensions : x, y et z.

+ [Boucles imbriquées](https://editor.p5js.org/WorkshopsB1/sketches/8SQp71gxE)
+ [Boucles imbriquées dans la boucle draw()](https://editor.p5js.org/WorkshopsB1/sketches/K73eLXpIq)

```js
function setup() {
  createCanvas(windowWidth, windowHeight);
  background(220);
}

function draw() {
  for (var y = 0; y < height; y = y + 20) {
    for (var x = 0; x < width; x = x + 20) {
        fill(random(255), random(255), random(255));
        rect(x, y, 15, 15);
    }
  }
}

```

