---
title: Les formes simples
lang: fr-FR
---

# Formes simples

Il existe de nombreux parallèles entre le dessin dans Processing et ce que vous faites ou avez déjà fait avec Illustrator. 

Chaque fois que vous tracez une forme dans un logiciel comme Illustrator ou Inkscape, du code est généré. Ce code définit les coordonnées des points, [couleur de remplissage](https://processing.org/reference/fill_.html) ou [de contour](https://processing.org/reference/stroke_.html), épaisseurs de contours, etc. Tous les pramètres de votre dessin y sont ainsi listés. 

Le principe est le même, par exemple, pour ce bout de code d'un fichier SVG :

<a data-fancybox title="SVG" href="/svg.png">![SVG](/svg.png)</a>

```javascript
void setup() {
  size(300,200);
  noStroke();
  fill(15, 127, 19);
  rect(0,70,100,80);
  stroke(255,0,0);
  line(5,5,250,95);
  fill(0,0,255);
  noStroke();
  ellipse(90,80,100,100);
  fill(0);
  text("Un texte", 180,60);
}
```

Dans Processing, nous dessinerons dans un premier temps à l'aide de fonctions telles rect(), ellipse(), triangle(), fill(), stroke(), strokeWeight(), noStroke(), noFill(), etc..

<a data-fancybox title="" href="/formes-simples-1.png">![](/formes-simples-1.png)</a>

```javascript
void setup() {
    size(120,120);
    smooth();

    fill(250,226,50);
    ellipse(80,80,40,40);

    noStroke();
    fill(0,162,255);
    rect(20,10,50,20);

    fill(238,34,12,128);
    rect(80,40,10,40);
}
```

Voici la liste des principales fonctions pour le dessin de formes dans Processing :

+ **rect()** : dessine un rectangle (ou un carré)
+ **ellipse()** : dessine une éllipse (ou un cercle)
+ **line()** : sans commentaire
+ **triangle()**
+ **point()**
+ **stroke()** : définit une couleur de contour
+ **strokeWeight()** : définit une épaisseur de contour
+ **noStroke()** : pas de contour
+ **fill()** : définit une couleur de remplissage
+ **noFill()** : pas de remplissage
+ **beginShape(), vertex(), endShape()** : vous permettent de dessiner des formes libres personnalisées
+ **bezier()** : pour tracer des courbes de Bezier. D'une certaine manière, cela vous permettra d'expérimenter la face cachée d'Illustrator...
+ etc... (fouillez un peu la [documetation Processing](https://processing.org/reference/))

<a data-fancybox title="" href="/illustrator.png">![](/illustrator.png)</a>

<a data-fancybox title="" href="/Ikko-Tanaka.jpg">![](/Ikko-Tanaka.jpg)</a>

-----

## En savoir plus :

+ [https://fr.flossmanuals.net/processing/les-formes/](https://fr.flossmanuals.net/processing/les-formes/)