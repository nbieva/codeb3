---
title: 4 - Les tableaux en pratique
display: Les tableaux en pratique
lang: fr-FR
---

<h1>{{ $frontmatter.display }}</h1>

<p class="lead">Quelques cas de mise en pratique de l'utilisation des tableaux.</p>

Un tableau (ou liste) est une **suite de variables**. Vous croiserez souvent sa syntaxe lors de vos recherches. Chaque tableau ne peut contenir que des variables **du même type**. il est extrêmement utile de savoir repérer un tableau dans un programme et de savoir les manipuler un minimum.
Chaque item du tableau est **accessible via son index** (0, 1, 2, 3, ..), entouré de crochets []. Nous verrons, avec javascript, que tout peut être variable (fonctions, nombres, chaînes de caractères, objets, ... )

## Les liens du jour

1. [ChatGPT](https://chat.openai.com/chat).
2. [Codex Javascript Sandbox](https://beta.openai.com/codex-javascript-sandbox)
3. Les [Objets et propriétés](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Working_with_Objects)

## Au programme

+ On revoit: **valeurs aléatoires** + **boucles for** + **Tableaux**
+ La **concaténation**: cas concrets
+ **Exports** et sauvegardes
+ XR6 Arrays: Stocker la position de la souris
+ XR6 Arrays: Cadavre exquis
+ XR6: Charger un tableau d'images et le réutiliser

## Dessin invisible

Créer un sketch dans lequel vous dessinez à l'aveugle puis, en appuyant sur une touche, vous voyez votre dessin apparaître. Il faudra donc pour cela stocker la position de la souris dans un tableau pour pouvoir aller la rechercher par après.

## Cadavre exquis

Créez un **cadavre exquis** à partir de différents tableaux (sujets, verbes, compléments). Une phrase doit apparaître à chaque clic de souris, à l'endroit du clic. Le rendu visuel est laissé à votre libre appréciation. 

Afin de trouver des listes de sujets, verbes etc.. vous pouvez vous aider de Google ou d'outils IA comme ChatGPT

## Tableau d'images

Remplissez un tableau d'une bonne dizaine d'images et faites en sorte que les images s'affichent quand on clique dans la page, à l'emplacement où l'on clique.



