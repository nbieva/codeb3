---
title: Supabase
date: 25/02/23
slug: sockets
disabled: false
thumb: supabase-thumb.jpg
active: true
current: true
ranking: 1
extrait: On teste les différentes solutions de bases de données. Ici Supabase.
featured:
    path: https://socket.io/fr/assets/images/basic-crud-app-55c1d48f52d1b78478b4f7b6a43dd7fd.gif
    absolute: true
    caption: Le principe des web sockets

---

+ [Supabase](https://supabase.com/)

## Database

Nous allons créer une simple application de type CRUD (CReate, Read, Update, Delete).

On va commencer par mettre en place un petit projet Vue.js. Mais le principe est le même avec d'autres environnements. Pour créer votre premier projet Vue3, rendez-vous sur le [Quick start de Vue.js](https://vuejs.org/guide/quick-start.html#creating-a-vue-application).

Ensuite, à la racine de notre projet, nous créons un fichier pour contenir nos variables d'environnement **.env** avec nos clés supabase:

```js
VITE_SUPABASE_URL=https://xfdgfdgfdwgdfgpmmkexu.supabase.co
VITE_SUPABASE_ANON_KEY=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVfggfdgdfggfzcHd6YWtwbW1rZXh1Iiwicm9sZSI6ImFub24dgffdgsdfgfgCixqkEG1kD4hASbOGCkGk1bDr1CAS2J2Y
```

Nous pouvons maintenant installer Supabase dans notre projet avec la commande suivante:

```bash
npm install @supabase/supabase-js
```

Pour connecter notre application avec Supabase nous allons dans notre Dashboard, dans l'admin de Supabase. On clique sur API docs. EN bas à droite, on devrait trouver un petit snippet, un petit bout de code qui ressemble à ceci:

```js
import { createClient } from '@supabase/supabase-js'

const supabaseUrl = 'https://xfdgfdgfdwgdfgpmmkexu.supabase.co'
const supabaseKey = process.env.SUPABASE_KEY
const supabase = createClient(supabaseUrl, supabaseKey)
```
On crée un nouveau dossier config dans le dossier src de notre application et on crée dans ce dossier un fichier supabaseClient.js avec le code ci-dessus. (Vous pouvez mettre ce fichier n'importe où et le nommer comme vous voulez)

On va juste renommer les variables dans ce fichier pour qu'elles correspondent aux nôtres.

```js
const supabaseUrl = import.meta.env.VITE_SUPABASE_URL
const supabaseAnonKey = import.meta.env.VITE_SUPABASE_ANON_KEY
```

Ensuite on va ajouter la ligne suivante qui va rendre cette communication avec la base de donnée disponible pour les autres modules.

```js
export const supabase = createClient(supabaseUrl, supabaseAnonKey)
```

Notre fichier ressemble maintenant à ceci:

```js
const supabaseUrl = import.meta.env.VITE_SUPABASE_URL
const supabaseAnonKey = import.meta.env.VITE_SUPABASE_ANON_KEY

export const supabase = createClient(supabaseUrl, supabaseAnonKey)
```

On va laisser le RLS et le REaltime décochés pour l'instant.

La Primary Key est l'identifiant unique qui permet d'identifier un élément par rapport à un autre

