---
title: 5 - Git, Github, GitLab & Co
display: Git, Github, GitLab & Co
lang: fr-FR
---

<h1>{{ $frontmatter.display }}</h1>

<p class="lead">Git n'est pas évident à prendre en main mais, parfois, il s'impose et on ne peut pas faire sans. Pour l'instant, **vous devez savoir ce que sont Git et Github** par exemple, **et à quoi il servent**. la prise en main et la maîtrise des outils viendront probablement plus tard.</p>

+ Créer un dépôt Git pour stocker nos projets

## Les liens du jour

+ [Les bases de Git](http://rogerdudler.github.io/git-guide/index.fr.html)
+ [La série de tutos de Grafikart sur Git](https://www.grafikart.fr/formations/git)
+ [NPM] (https://la-cascade.io/articles/npm-guide-complet-pour-le-debutant)
+ [Command line basics](https://www.courses.tegabrain.com/cc18/github-and-command-line-basics/)


<!-- 
Pour la suite:

+ Manipuler le DOM (1) : créer
+ Manipuler le DOM (2) : interagir
+ Travailler avec des données : le format JSON
+ Travailler avec des données : les APIs
+ Machine learning et AI (1)
+ Machine learning et AI (2)
+ Interfaces
+ WebGL, Smartphones, accelerometres et gyroscopes
+ (Python)
+ (Terminal)
 -->

## La gestion de version avec GIT

<img src="/gitlogo.png" style="float:right;width:150px;margin:0 0 1.2rem 1.8rem;">Si vous travaillez sur de plus importants projets, ou si vous collaborez avec d'autres personnes, ou dans tous les autres cas d'ailleurs, vous vous frotterez à un moment ou à un autre à un **outil de gestion de versions** comme [git](https://fr.wikipedia.org/wiki/Git). Dans vos recherches sur le web, vous serez d'ailleurs régulièrement amenés à consulter des dépôts (repositories) sur [Github](https://github.com/), [Gitlab](https://about.gitlab.com/), [Bitbucket](https://bitbucket.org/) ou [autres](https://git-scm.com/download/gui/windows)..

Git n'est pas évident à prendre en main mais, parfois, il s'impose et on ne peut pas faire sans. Pour l'instant, **vous devez savoir ce que sont Git et Github** par exemple, **et à quoi il servent**. la prise en main et la maîtrise des outils viendront probablement plus tard.

<!--
<a data-fancybox title="P5js" href="/git.png">![P5js](/git.png)</a>
-->
## Github, GitLab, Bitbucket ..

Ces différents sites, que vous croiserez régulièrement, sont des lieux où vous pouvez, à l'instar de milliers de personnes, **héberger le code de vos projets** dans ce que l'on appelle des **dépôts (repositories)**.

Vous pouvez également **cloner** les dépôts d'autres développeurs ou artistes sur votre propre machine.

+ [Gitlab](https://gitlab.com/)
+ [Github](https://github.com/)
+ [Bitbucket](https://bitbucket.org/)





### Sourcetree

+ [https://www.sourcetreeapp.com/](https://www.sourcetreeapp.com/)

