---
title: 2 - L'élément canvas
display: L'élément canvas
lang: fr-FR
---

<h1>{{ $frontmatter.display }}</h1>

<p class="lead">L'élément HTML Canvas a fait son apparition avec HTML5, en même temps qu'un grand nombre d'autres éléments (main, header, footer, aside ...), dont certains en corrélation étroite avec Javascript (video, audio, drag and drop, canvas ...). Il permet, à l'aide de Javascript donc, de dessiner, créer ou manipuler des contenus graphiques directement dans la page web.</p>


<a data-fancybox class="featured" title="canvas" href="https://assets.website-files.com/61e6c06a23cb13bf76051194/620679c758afd14dc184463e_IMG_0155.PNG">![canvas](https://assets.website-files.com/61e6c06a23cb13bf76051194/620679c758afd14dc184463e_IMG_0155.PNG)</a> 
> **Zancan**, Lushtemples — Ars Ex Naturae Ex, 2021 / Voir aussi sa série [Garden Monoliths](https://www.fxhash.xyz/generative/slug/garden-monoliths)
## Les liens et artistes du jour

1. [Processing](https://processing.org/)
2. [P5js.org](https://p5js.org/) + [https://p5js.org/get-started/](https://p5js.org/get-started/) + [P5js Cheat Sheet](https://bmoren.github.io/p5js-cheat-sheet/)
3. Les **bibliothèques** (libraries) JS (Ex [P5js](https://p5js.org/libraries/))
4.  HTML [Canvas](https://www.w3schools.com/html/html5_canvas.asp)
5. [https://www.fxhash.xyz/](https://www.fxhash.xyz/)
6. [Randomness in art](https://www.courses.tegabrain.com/cc18/rand[omness-in-art/)
7. [Garden monolith by Zancan](https://www.fxhash.xyz/generative/slug/garden-monoliths)
8. [Painting with code (g) and machines](https://twitter.com/0xfar/status/1595550611566649346/photo/1)
9.  [Rafael Rozendaal](https://twitter.com/newrafael/status/1595529779977019395/photo/1) + [site](https://www.newrafael.com/)
10. [Jan Robert Leegte](https://www.leegte.org/)
11. [Leander Herzog](https://twitter.com/lennyjpg/status/1595348442741723138/photo/1)
12. ['Etched clouds in P5js'](https://twitter.com/generativelight/status/1592699544579735553/photo/1)
13. [Meridian by Matt DesLauriers](https://opensea.io/collection/meridian-by-matt-deslauriers) + [Notes](https://mattdesl.substack.com/p/notes-on-meridian) + [Edition](https://vetroeditions.com/products/meridian)
14. [Another example](https://www.fxhash.xyz/generative/slug/acequia)
15. [Fira Code (font)](https://github.com/tonsky/FiraCode)

## Au programme aujourd'hui

+ Retour sur notre premier cours manqué..
+ Dessin dans la page web et **Design génératif**
+ Rappel des concepts déjà évoqués en B1: **fonctions**, **Variables**, **Boucles**, **Conditions** ...
+ Exercices

## Exercices

+ [Pierre **Bismuth**](exercices/drapeau.md) (base + mode colorimétrique)
+ Outil de **dessin** + sauvegarde (interactivité souris)
+ **Générateur** de drapeaux (aléatoire et autonomie)
+ **10 print** (boucles et conditions)

<a data-fancybox title="canvas" class="fullwidth" href="https://artlogic-res.cloudinary.com/w_1000,h_1000,c_limit,f_auto,fl_lossy,q_auto/ws-galerieboulakia/usr/images/artists/artist_image/items/f2/f26a30579100497bb831c41fc7060dc3/hans_hartung_14.jpg">![canvas](https://artlogic-res.cloudinary.com/w_1000,h_1000,c_limit,f_auto,fl_lossy,q_auto/ws-galerieboulakia/usr/images/artists/artist_image/items/f2/f26a30579100497bb831c41fc7060dc3/hans_hartung_14.jpg)</a>
> **Hans Hartung** construisait ses propres outils



<!-- ## 10 print

+ https://editor.p5js.org/codingtrain/sketches/ryxWYgmwX -->

<!-- 

```javascript
var monTexte = ["Hello", "Bonjour", "Holà", "Sayonara", "Hei!"];
var index;

function setup() {
  createCanvas(windowWidth, windowHeight);
  index = floor(random(monTexte.length));
  text(monTexte[index], 100,100);
  print(monTexte.length);
}
```

### Gradient

```javascript
function setup() {
  createCanvas(800, 600);
  //background(50);
}

function draw() {
  loadPixels();
  for(let x=0;x<width;x++) {
    for(let y =0;y<height;y++) {
      let pospix = (x+y*width)*4;
      pixels[pospix] = x/2;
      pixels[pospix+1] = y/2;
      pixels[pospix+2] = 100;
      pixels[pospix+3] = 255;
    }
  }
  updatePixels();
}
```

### Drop images

```javascript
var mesImages = [];
var r;

function preload() {
    for (var i = 0; i < 8; i++) {
        mesImages[i] = loadImage("images/image_" + i + ".jpg");
    }
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    frameRate(3);
    print(mesImages);
}

function draw() {
   /* if (mouseIsPressed) {
        
    }*/
}
function mouseClicked() {
    r = floor(random(8));
    image(mesImages[r], mouseX, mouseY);
    print(r);
}
``` -->