---
title: Vite
date: 25/02/23
slug: sockets
disabled: false
thumb: supabase-thumb.jpg
active: true
current: true
ranking: 1
extrait: On teste les différentes solutions de bases de données. Ici Supabase.
featured:
    path: https://socket.io/fr/assets/images/basic-crud-app-55c1d48f52d1b78478b4f7b6a43dd7fd.gif
    absolute: true
    caption: Le principe des web sockets

---

+ [Supabase](https://supabase.com/)

## Database

On crée Vite:

```bash
npm create vite@latest
```

Ensuite on installe les outils de ligne de commande de supabase:

```bash
npm i -g supabase
```

sur mac: brew install supabase/tap/supabase


Ajoutez sudo devant si nécessaire