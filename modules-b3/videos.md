---
title: Les vidéos
display: Les vidéos
lang: fr-FR
---

<h1>{{ $frontmatter.display }}</h1>

<p class="lead">Une page pour regrouper les différentes vidéos du cours. Ces vidéos ne sont pas supposées reprendre l'intégralité de ce que nous avons vu ou allons voir. Elles servent à compléter les cours, et à vous aider sur des aspects techniques sur lesquels on a parfois besoin de revenir à son aise..</p>

[[toc]]

## 1. Editeur de code et environnement de travail

Présentation sommaire de nos différents outils et plus particulièrement de Visual Studio Code (Interface, thèmes, extensions...)

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://www.youtube.com/embed/uX1ruAvou3U?vq=hd1080&rel=0"  style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" title="Éditeur de code et environnement de travail" allowfullscreen></iframe></div>

## 2. HTML et CSS en très très très résumé

Petite présentation très résumée de ce que sont HTML et CSS. **Cette vidéo s'adresse évidemment plutôt à ceux et celles qui n'ont pas suivi les cours de Web en B2**

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://www.youtube.com/embed/UPtaibLdQpo?vq=hd1080&rel=0"  style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" title="Éditeur de code et environnement de travail" allowfullscreen></iframe></div>

## 3. Git et Github (et Netlify)

Première tentative de présentation de Git et Github, et comment nous pouvons les utiliser pour simplifier la mise à jour de nos projets sur un serveur distant (dans ce cas-ci hébergé sur Netlify)

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://www.youtube.com/embed/DtT3j5omk4c?vq=hd1080&rel=0"  style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" title="Éditeur de code et environnement de travail" allowfullscreen></iframe></div>

## 4. Le langage Markdown

Petite présentation (c'est plus pour info) du langage Markdown créé par JOhn Gruber et Aaron Swartz. Un langage dont, personnellement, je ne me passe plus...

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://www.youtube.com/embed/d2HbWUzqx6Y?vq=hd1080&rel=0"  style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" title="Éditeur de code et environnement de travail" allowfullscreen></iframe></div>

## 5. Exercice introductif - Pierre Bismuth (1 et 2)

Où l'on reprend les différentes étapes pour récréer ce travail de Pierre Bismuth (le drapeau de gauche). L'exercice est accessible également via [cette page](/modules-b3/exercices/drapeau.md), où vous pourrez trouver l'image originale.

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://www.youtube.com/embed/AU0dBZYICO4?vq=hd1080&rel=0"  style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" title="Exercice introductif - Pierre Bismuth" allowfullscreen></iframe></div>

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://www.youtube.com/embed/XlW1-loP4Rw?vq=hd1080&rel=0"  style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" title="Exercice introductif - Pierre Bismuth" allowfullscreen></iframe></div>

## 6. Les boucles for

Petite présentation basique du principe des boucles for, qui vont nous faire gagner beaucoup de temps et donner un peu plus d'ampleur à nos sketches.

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://www.youtube.com/embed/6NbOpGTAB-8?vq=hd1080&rel=0"  style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" title="Exercice introductif - Pierre Bismuth" allowfullscreen></iframe></div>

## 7. Les Tableaux (Arrays)

Présentation des tableaux dans P5js. Que sont-ils? Comment les créer, les remplir ou aller y chercher des données?

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://www.youtube.com/embed/BmRq0bI8HKE?vq=hd1080&rel=0"  style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" title="Exercice introductif - Pierre Bismuth" allowfullscreen></iframe></div>

## 8. XR6: Cadavre exquis (ou plus ou moins)

On voit ici comment créer un générateur de phrases aléatoires basé sur 3 ou 4 tableaux (sujets, verbes, etc.).

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://www.youtube.com/embed/kumecsMMPHk?vq=hd1080&rel=0"  style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" title="Cadavre exquis (ou plus ou moins)" allowfullscreen></iframe></div>

## 9. XR6: Remplir un tableau (array) avec des images

On voit ici comment remplir un tableau (array) avec des images pour venir ensuite les rechercher et les déposer dans notre page web.

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://www.youtube.com/embed/URzf8O8sbfo?vq=hd1080&rel=0"  style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" title="Remplir un tableau (array) avec des images" allowfullscreen></iframe></div>