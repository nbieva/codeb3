---
title: 3 - Les tableaux (Arrays)
display: Les tableaux (Arrays)
lang: fr-FR
---

<h1>{{ $frontmatter.display }}</h1>

<p class="lead">Introduction aux tableaux (arrays) et à leur manipulation dans Processing ou P5js. Import et utilisation d'images dans un sketch, etc..</p>

<a data-fancybox title="Pixels" href="/array.png">![Pixels](/array.png)</a>

Un tableau (ou liste) est une **suite de variables**. Vous croiserez souvent sa syntaxe lors de vos recherches. Chaque tableau ne peut contenir que des variables **du même type**. il est extrêmement utile de savoir repérer un tableau dans un programme et de savoir les manipuler un minimum.
Chaque item du tableau est **accessible via son index** (0, 1, 2, 3, ..), entouré de crochets []. Nous verrons, avec javascript, que tout peut être variable (fonctions, nombres, chaînes de caractères, objets, ... )

## Les liens et artistes du jour

1. [Cheval vert](https://chevalvert.fr/en/projects/murmur)
2. [Exemples P5js (Lauren McCarthy)](https://github.com/lmccart/gswp5.js-code)
3. [ChatGPT](https://chat.openai.com/chat), un outil qui risque bien de changer nos façons d'apprendre et d'explorer.
## Au programme

+ On revoit: **valeurs aléatoires** + **boucles for**
+ Les **tableaux** (Arrays)
+ La **concaténation**
+ **Exports** et sauvegardes
+ XR6 Arrays: Cadavre exquis
+ XR6 Arrays: Stocker la position de la souris
+ XR6: Charger un tableau d'images et le réutiliser
## Exercices

+ Créez un **cadavre exquis** à partir de différents tableaux (sujets, verbes, compléments). Une phrase doit apparaître à chaque clic de souris, à l'enroit du clic. Le rendu visuel est laissé à votre libre appréciation. + export ds fichier
+ Créer un sketch dans lequel vous dessinez à l'aveugle puis, en appuyant sur une touche, vous voyez votre dessin.

## Les tableaux dans P5js

Bien que le principe reste le même dans les différents langages, la syntaxe diffère bien souvent. Vous trouverez ci-dessous quelques exemples en utilisant javascript (p5js). Un tableau peut contenir tout type de données (texte, nombres, images, sons, objets..).

### Remplir un tableau

Dès que l'on travaille avec un grand nombre de données, les boucles for s'avèrent vite indispensables pour gérer les tableaux. La propriété length correspond au nombre d'éléments qui composent le tableau, le premier occupant la position 0.

```javascript
let montableau = [];

function setup() {
  createCanvas(600, 400);
  for (let i = 0; i < 50; i++) {
    let rand = random(100);
    montableau[i] = rand;
  }
  console.log("Il y a "+ montableau.length + " éléments dans mon tableau"); // Affiche: Il y a 50 éléments dans mon tableau
}
```

### Accéder aux éléments du tableau:

On accède aux différents éléments du tableau en notant leur position entre crochets. On appelle cette position l'**index** de l'élément:

```javascript
console.log(montableau[0]); // Affichera le premier élément du tableau (occupant la position 0)
```
Dans un tableau d'une longueur de 50, il n'y a donc aucun élément occupant la 50e position. La dernière position du tableau étant la 49e (vu que l'on commence à compter à 0).

```javascript
let montableau = [];

function setup() {
  createCanvas(600, 400);
  for (let i = 0; i < 50; i++) {
    let rand = random(100);
    montableau[i] = rand;
  }
  console.log("Il y a "+ montableau.length + " éléments dans mon tableau"); // Affiche: Il y a 50 éléments dans mon tableau
}
```

Un tableau peut contenir des données de types différents, comme ici:

```javascript
let montableau = ["Un texte", 12, 489.569802456874, false, "Mais encore", {nom: "Dupont", prenom: "Nicolas"}];

function setup() {
  createCanvas(600, 400);
  console.log(montableau[4]); // Affiche Mais encore
}
```

### push : Ajouter des éléments à un tableau

Pour ajouter des éléments à un tableau, utilisez la [fonction push](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push).

Comme dans l'exemple ci-dessous, pour stocker la position de la souris:

```javascript
let positions = [];

function setup() {
  createCanvas(800, 800);
  background(220);
}

function draw() {
  if(mouseIsPressed) {
    positions.push(mouseX, mouseY);
  }
  if(keyIsPressed) {
    console.log(positions);
    for(let i=0;i<positions.length-1; i+=2) {
        line(positions[i], positions[i+1], positions[i+2], positions[i+3]);
    }
  }
}
```

### Modifier le contenu d'un tableau

La méthode [splice()](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/splice) modifie le contenu d'un tableau en retirant des éléments et/ou en ajoutant de nouveaux éléments à même le tableau.On peut ainsi vider ou remplacer une partie d'un tableau.

```javascript
var mesPoissons  = ["scalaire", "clown", "mandarin", "chirurgien"];

// supprime 0 élément à partir de l'index 2, et insère "tambour"
var enleves = mesPoissons.splice(2, 0, "tambour");
// mesPoissons est ["scalaire", "clown", "tambour", "mandarin", "chirurgien"]
// enleves est [], aucun élément supprimé

// supprime 1 élément à partir de l'index 3
enleves = mesPoissons.splice(3, 1);
// mesPoissons est ["scalaire", "clown", "tambour", "chirurgien"]
// enleves est ["mandarin"]

// supprime 1 élément à partir de l'index 2, et insère "trompette"
enleves = mesPoissons.splice(2, 1, "trompette");
// mesPoissons est ["scalaire", "clown", "trompette", "chirurgien"]
// enleves est ["tambour"]

// supprime 2 éléments à partir de l'index 0, et insère "perroquet", "anémone" et"bleu"
enleves = mesPoissons.splice(0, 2, "perroquet", "anémone", "bleu");
// mesPoissons est ["perroquet", "anémone", "bleu", "trompette", "chirurgien"]
// enleves est ["scalaire", "clown"]

// supprime 2 éléments à partir de l'indice 2
enleves = mesPoissons.splice(mesPoissons.length - 3, 2);
// mesPoissons est ["perroquet", "anémone", "chirurgien"]
// enleves est ["bleu", "trompette"]

var mesPoissons = ["perroquet", "anémone", "bleu", "trompette", "chirurgien"];
// on retire trois éléments à partir de l'indice 2
enleves = mesPoissons.splice(2);
// mesPoissons vaut ["perroquet", "anémone"]
// enleves vaut ["bleu", "trompette", "chirurgien"]

var mesAnimaux = ["cheval", "chien", "chat", "dauphin"];
enleves = mesAnimaux.splice(-2, 1);

// mesAnimaux vaut ["cheval", "chien", "dauphin"]
// enleves vaut ["chat"]
```











<!-- 


## Les tableaux dans Processing

```javascript
int[] values = {8, 9, 10, 11};

void setup() {
  size(800, 600);
  text(values[2],50,50);
}
```

```javascript
int[] values = {5,-3,78,5,-5896,4};

void setup() {
  size(800, 600);
  for(int i=0;i<values.length;i++) {
      prinln(values[i]);
  }
}
```

```javascript
int[] values;

void setup() {
  size(800, 600);
  values = new int[8];
  for(int i=0;i<values.length;i++) {
      prinln(values[i]);
  }
}
```

### Tableaux de chaînes de caractères

```javascript
String[] sujets = {"Vert","Rouge","Jaune","Bleu"};
float posx, posy;

void setup() {
  size(800, 600);
  background(255);
  noStroke();
  frameRate(2);
  textSize(18);
}

void draw() {
  fill(255,80);
  rect(0,0,width,height);
  fill(0);
  for(int i=0;i<sujets.length;i++) {
    posx = random(width);
    posy = random(height);
    text(sujets[i], posx, posy);
  }
}
```

::: warning
Réalisez un cadavre exquis en utilisant 3 tableaux différents (sujets, verbes, compléments) de 10 entrées chacun. Une phrase différente doit apparaître chaque seconde.
:::

+ https://www.processing.org/tutorials/arrays/
+ https://www.processing.org/tutorials/2darray/

### Enregistrer des données

```javascript
// Définittion du nombre de points à enregistrer (à stocker dans les tableaux)
int num = 75;
int[] x = new int[num];
int[] y = new int[num];

void setup() { 
  size(800, 800);
  noStroke();
  fill(255, 102);
}

void draw() {
  background(0);
  // Décale les valeurs du tableau vers la droite
  for (int i = num-1; i > 0; i--) {
    x[i] = x[i-1];
    y[i] = y[i-1];
  }
  // Ajoute les nouvelles valeurs au début des tableaux
  x[0] = mouseX;
  y[0] = mouseY;
  
  // Dessine les cercles en parcourant les tableaux
  for (int i = 0; i < num; i++) {
    ellipse(x[i], y[i], i, i);
  }
}
```

---------

+ Les **fonctions personnalisées**
+ P5js
+ [HTML, CSS](https://webb2.netlify.com/) etc
+ L'[élément Canvas](https://www.w3schools.com/html/html5_canvas.asp)
  
## Les images

### Charger et afficher une image

```javascript
PImage img; // déclaration de la variable image

void setup() {
    size(600, 600);    
    img = loadImage("workshop.jpg");
}

void draw() {
    image(img, 0, 0, width/2, height/2);
}
```

+ preload()




## Emma, ton code:

```javascript
function setup() {
    createCanvas(windowWidth,windowHeight);
    background(10,0,0);
    stroke(0,30);
}
function draw() {
    star(width/2, height/2, 40, mouseY, mouseX);
}

function star(x, y, radius1, radius2, npoints) {
    let angle = TWO_PI / npoints;
    let halfAngle = angle / 2.0;
    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
      let sx = x + cos(a) * radius2;
      let sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a + halfAngle) * radius1;
      sy = y + sin(a + halfAngle) * radius1;
      vertex(sx, sy);
    }
    endShape(CLOSE);
  }

function keyPressed() {
    if (hour()<12) {
        save("Capture d'étoile-"+hour()+"-"+minute()+"-"+second()+".jpg");
    }
}
``` -->