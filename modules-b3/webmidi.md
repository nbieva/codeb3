---
title: Web & Midi
date: 25/02/23
slug: sockets
disabled: false
thumb: sockets-thumb.jpg
active: true
current: true
ranking: 1
extrait: Le but est de pouvoir connecter plusieurs machines d'un même réseau pour créer le même principe qu'un chat ou un outil de dessin collaboratif. L'étape 2 consiste à déployer l'application sur un serveur de type DigitalOcean, etc.
featured:
    path: https://socket.io/fr/assets/images/basic-crud-app-55c1d48f52d1b78478b4f7b6a43dd7fd.gif
    absolute: true
    caption: Le principe des web sockets

---

+ [Webmidi.js](https://webmidijs.org/)

### Webmidi

Le but est de pouvoir connecter plusieurs machines d'un même réseau pour créer le même principe qu'un chat ou un outil de dessin collaboratif. L'étape 2 consiste à déployer l'application sur un serveur de type DigitalOcean, etc.


```js
{
  "name": "Drawing together",
  "version": "0.1",
  "description": "Une application pour dessiner ensemble.",
  "dependencies": {}
}
```
A présent, nous pouvons installer l'un ou l'autre module en utilisant notre terminal. Commençons par installer Express.js, un framework nous permettant de créer très rapidement de petites applications web et de gérer le côté serveur.

Au lieu de npm, vous pouvez également utiliser Yarn. La syntaxe est très légèrement différente mais le principe reste le même.

```js
npm install express
```
