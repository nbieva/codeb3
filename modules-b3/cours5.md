---
title: 5 - Manipuler le DOM
display: Manipuler le DOM
lang: fr-FR
---

<h1>{{ $frontmatter.display }}</h1>

<p class="lead">La page web est composée d'une série d'éléments imbriqués les uns dans les autres ou placés côte à côte. A l'aide de javaScript, et plus spécifiquement jQuery, P5js, Vue.js ou d'autres frameworks ou bibliothèques, nous pouvons manipuler ces éléments, les modifier, les supprimer ou en créer de nouveaux.</p>

<video class="videohtml" controls autoplay>
    <source src="/rafman.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video> 

> [John Rafman, Natura morta](http://natura-morta.com/)


+ [https://genuary.art/](https://genuary.art/)
+ Qu'est-ce que le DOM?
+ jQuery/P5js/Bootstrap/Vue.js
+ Intégrer jQuery à son projet (pour info)
+ Intégrer une bibliothèque externe comme Fancybox (et pourquoi?)
+ XR6: Show/hide + Fancybox
+ Création et manipulation d'éléments HTML
+ Modification des règles CSS avec JS
+ Manipulation du contenu ou de la structure de la page (aussi Flexbox)

```javascript
function setup() {
  noCanvas();
}

function draw() {
  createDiv('Everything is getting better every day');
}
```

## Créations d'interfaces

+ Différentes façons de créer de l'interaction
+ HTML inputs, Buttons et autres éléments de formulaires
+ Récupérer les valeurs: https://p5js.org/examples/dom-input-and-button.html
+ les utiliser dans la page web
+ P5 GUI: https://github.com/bitcraftlab/p5.gui
+ XR6: Création d'une interface
+ XR6: Visualiser le temps

## Exercices

+ Changer la classe d'un élément lorsque l'on clique dessus.
+ Modifier la couleur d'arrière-plan (ou l'image) d'une page web à chaque chargement.
+ + Modifier la couleur d'arrière-plan (ou l'image) d'une page web en utilisant un input color.
