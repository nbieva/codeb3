---
title: Firebase
date: 25/02/23
slug: sockets
disabled: false
thumb: sockets-thumb.jpg
active: true
current: true
ranking: 1
extrait: Le but est de pouvoir connecter plusieurs machines d'un même réseau pour créer le même principe qu'un chat ou un outil de dessin collaboratif. L'étape 2 consiste à déployer l'application sur un serveur de type DigitalOcean, etc.
featured:
    path: openstreetmap.jpg
    absolute:
    caption: L'interface d'édition d'OpenStreetMap

---

Le but était d'ici de parvenir à mettre en place une application qui puisse communiquer avec une base de données pour que l'on puisse à la fois stocker des données émises par le client (dans un champ de formulaire par exemple) et à la fois récupérer les données stockées au préalable lors du premier chargemet de l'application.

Bonus: Mettre cette communication bidirectionnelle en place mais en temps réel en utilisant les variantes Realtime d'outils comme **Firebase** ou **Supabase**.

# Firebase

## Installation

```bash
npm i firebase
```

Une fois l'installation terminée, nous aurons accès à la bibliothèque Firebase.
A la racine de mon projet, dans mon fichier index.js, j'ajoute les éléments suivants:

```js
import { initializeApp } from 'firebase/app'
```

Quand vous créez votre application Firebase via la console Google, Google vous donne un fichier de configuration avec apiKey, etc.. Il ressemble à ceci:

```js
const firebaseConfig = {
    apiKey: "AIzaSyC9q14BiHrHRxytxYIRyKUj6YclmxESJTE",
    authDomain: "crossdrawing-6daa5.firebaseapp.com",
    databaseURL: "https://crossdrawing-6daa5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "crossdrawing-6daa5",
    storageBucket: "crossdrawing-6daa5.appspot.com",
    messagingSenderId: "423156914339",
    appId: "1:423156914339:web:d2dc2c536612bdeb764716"
}
```

---------------

## SASS

+ creer le fichier html
+ créer styles.scss

Si SASS n'est pas encore installé sur le système
```bash
npm install -g node-sass 
```

```bash
npm init 
```

Dans l'objet scripts de package.json :

```json
"scss": "node-sass --watch assets/scss -o assets/css"
```

Lancer la commande
```bash
npm run scss //ou yarn scss
```

Maintenant que le HTML et SASS sont en places...

Installer vue
```bash
npm install -g @vue/cli
```

Ajouter sudo devant la commande si nécessaire

https://www.youtube.com/watch?v=lcYn0tgUvHE

yarn serve