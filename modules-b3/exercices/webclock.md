---
title: Exercice - Web clocks
display: Exercice - Web clocks
lang: fr-FR
---

<h1>{{ $frontmatter.display }}</h1>

<p class="lead">Cet exercice fait usage de différents éléments autour de l'usage d'un fichier JSON. Ce type de fichier fait office de petite base de données pour notre page, comme on peut d'ailleurs le faire avec un fichier CSV également..</p>

<!-- <a data-fancybox title="JSON" href="/json.jpg">![](/json.jpg)</a>
> Un exemple de fichier de test JSON (Source: https://jsonplaceholder.typicode.com/) -->

<iframe class="p5embed" width="840" height="400" src="https://editor.p5js.org/nicobiev/embed/cWneuXsNd"></iframe>

[[toc]]

## 1. Canvas clock

P5js comprend quelques fonctions permettant de récupérer en temps réel l'heure ( **hour()** ), la minute ( **minute()** ), la seconde ( **second()** ) et la milliseconde ( **millis()** ).

A partir de là, vous pouvez tout imaginer pour créer une sorte d'horloge pour visualiser le temps qui passe d'une façon inédite. Cela peut-être créer un sketch purement visuel, ou manipuler les valeurs numériques (ce que renvoient ces fonctions) pour brouiller les pistes, etc..

Vous trouverez ci-dessous un code d'exemple (qui n'est certainement pas un modèle) avec ces différentes variables.

## Exemple (pas un modèle)

```javascript
function setup() {
  createCanvas(windowWidth,windowHeight); // mais pourrait aussi être 800,600
  noStroke();
}

function draw() {
  background(0, 122, 142);
  fill(76, 165, 71);
  rect(0,0,hour()*(width/24),height/3);
  fill(61, 121, 87);
  rect(0,height/3,minute()*(width/60),height/3);
  fill(64, 94, 104);
  rect(0,2*(height/3),second()*(width/60),height/3);
}
```

## 2. Web clock

Maintenant que vous avez compris le principe et créé quelque chose d'intéressant en utilisant la balise canvas, vous pouvez vous lancer dans la conception d'une web clock.

Il s'agit, toujours en utilisant ces valeurs numériques mises à notre disposition, de visualiser le temps en manipulant les éléments HTML eux-mêmes à l'aide de CSS et JS (display, taille, couleurs, positions, filtres, etc..)

## Liens divers

+ [http://golancourses.net/2015/lectures/visualizing-time/](http://golancourses.net/2015/lectures/visualizing-time/)
+ https://www.courses.tegabrain.com/cc18/visualizing-time/
+ [History of timekeeping devices](https://en.wikipedia.org/wiki/History_of_timekeeping_devices)
+ http://cmuems.com/2016/60212/lectures/lecture-09-09b-clocks/
+ http://cmuems.com/2016/60212/resources/drucker_timekeeping.pdf
+ [An entire history of time measurement in six minutes.](https://www.youtube.com/watch?v=SsULOvIWSUo) (frome [here](http://cmuems.com/2016/60212/deliverables/deliverables-02/)) !!!
+ [https://www.humanclock.com/](https://www.humanclock.com/)
+ Moment and P5js: [https://editor.p5js.org/kjhollen/sketches/B1t0ov4ab](https://editor.p5js.org/kjhollen/sketches/B1t0ov4ab)
+ [https://editor.p5js.org/nicobiev/present/cWneuXsNd](https://editor.p5js.org/nicobiev/present/cWneuXsNd)
+ [https://editor.p5js.org/NicolasB3/present/BhPqaJbNt](https://editor.p5js.org/NicolasB3/present/BhPqaJbNt)