---
title: Exercices - Data
display: Exercices - Data
lang: fr-FR
---

<h1>{{ $frontmatter.display }}</h1>

<p class="lead">Cet exercice fait usage de différents éléments autour de l'usage d'un fichier JSON. Ce type de fichier fait office de petite base de données pour notre page, comme on peut d'ailleurs le faire avec un fichier CSV également..</p>

<a data-fancybox title="JSON" href="/json.jpg">![](/json.jpg)</a>
> Un exemple de fichier de test JSON (Source: https://jsonplaceholder.typicode.com/)

A la façon d'une image ( loadImage ), d'un fichier CSV ( loadTable ) ou de la liste des pixels d'une imahge ou du canvas ( loadPixels ), le chargement d'un fichier JSON dans notre sketch peut se faire en utilisant [la fonction loadJSON](https://p5js.org/reference/#/p5/loadJSON).

Pour les exercices ci-dessous, vous êtez bien évidemment libres en ce qui concerne design. Tout dépend de ce que vous recherchez. On pourra évidemment évoquer ces aspects là au cours.

[[toc]]

## 1. Galerie photo

En utilisant [ce fichier d'exemple JSON](https://jsonplaceholder.typicode.com/albums/2/photos), créez une page web présentant une galerie photo. On doit voir une série de vignettes. Quand on clique sur une vignette, l'image originale associée doit s'afficher. Utilisez pour cela la bibliothèque [Fancybox (v4)](https://fancyapps.com/docs/ui/quick-start). Jetez un oeil à [la documentation](https://fancyapps.com/docs/ui/quick-start) pour comprendre comment l'intégrer et la faire fonctionner.

L'important ici est que nos données provienne d'un fichier JSON que nous allons chercher en ligne.

<video class="videohtml" src="/colors.mp4" controls autoplay loop />

## 2. Posts

Dans la même logique, mais plus orienté texte, créez une page web sur base de [ce fichier d'exemple JSON](https://jsonplaceholder.typicode.com/posts) présentant une liste de posts avec pour chacun le titre et la description affichées.

## 3. Wordpress API

Vous pouvez également utiliser l'API de Wordpress pour **recréer une version custom de la homepage d'un site** qui utilise ce CMS. La structure des données est un peu plus complexe mais c'est l'intérêt de cet exercice.
Digitalab en est un exemple.

<video class="videohtml" src="/digitalab.mp4" controls autoplay loop />

Pour obtenir la liste des posts publics d'un site tournant sous Wordpress et ayant laissé l'API disponible publiquement, vous devrez ajouter à la suite du domaine la structure suivante :

```txt
/wp-json/wp/v2/posts
```
C'est ce que l'on appelle un point d'entrée, ou *entry point*. Pour digitalab.be ce sera donc:

```txt
http://www.digitalab.be/wp-json/wp/v2/posts
```
Pour riverdance.com, ce sera donc ceci :-)

```txt
https://riverdance.com/wp-json/wp/v2/posts
```

Utilisez l'extension [JSON formatter](https://chrome.google.com/webstore/detail/json-formatter/bcjindcccaagfpapjjmafapmmgkkhgoa) ou équivalent pour rendre ces données intelligibles dans votre navigateur.

## 4. Votre propre source

Choisissez votre propre source [ici](https://github.com/dariusk/corpora/tree/master/data) ou ailleurs sur le web et imaginez un projet à partir de là. C'est de loin l'exercice le plus intéressant ;-)

## Référence

+ Création de projet
+ **setup()** et **draw()**
+ variables **width**, **height**, **windowWidth**, **windowHeight**, **frameCount**
+ Toute la référence concernant la création et la manipulation d'éléments HTML avec P5js
+ Eventuellement **random()**
+ **Boucles for()**
+ **conditions**
+ **Tableaux**
+ **loadJSON**
+ **Naviguer dans les objets et les tableaux**
+ Notions **HTML**
+ Notions **CSS**
+ **Mise en ligne**
+ ...