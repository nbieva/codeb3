---
title: Exercice - Pierre Bismuth
display: Exercice - Pierre Bismuth
lang: fr-FR
---

<h1>{{ $frontmatter.display }}</h1>

<p class="lead">Un premier exercice pour se replonger dans les fonctions de bases de P5.js abordées durant le workshop et/ou dans les cours de B2, et pour expérimenter la mise en ligne/mise à jour à l'aide de la ligne de commande et de Git.</p>

<a data-fancybox title="Pixels" href="/flag-empain.jpg">![](/flag-empain.jpg)</a>
> [Pierre Bismuth](https://fr.wikipedia.org/wiki/Pierre_Bismuth), Variations sur le thème des nations - [Flags à la Fondation Boghossian](https://www.villaempain.com/expo/flags/), Avenue Franklin Roosevelt

## Objectif

1) Réalisez le drapeau de gauche à l'aide de [P5.js](https://p5js.org/) de la façon la plus 'flexible' possible (en utilisant un maximum de variables ou d'opérations basées sur des variables) - Utilisez les couleurs les plus proches possibles.
2) Faites-en une version ou le rectangle est centré dans la page HTML et une autre ou il est en plein écran, flexible et adaptatif.
3) Mettez en ligne sur Netlify et envoyez-moi l'adresse par email



## Références

+ Création de projet
+ **setup()** et **draw()**
+ **rect()**
+ variables **width**, **height**, **windowWidth**, **windowHeight**, **frameCount**
+ Couleurs (**fill**, **noFill**, **stroke**, **noStroke**)
+ **colorMode()**
+ **random()**
+ **conditions**
+ Commandes Git basiques
+ **Mise en ligne**