---
title: 6 - Datas - le format JSON
display: Travailler avec des données - le format JSON
lang: fr-FR
---

<h1>{{ $frontmatter.display }}</h1>

<p class="lead">Dès que l'on travaille avec javascript, on tombe assez rapidement sur les objets et leur syntaxe particulière. Le format JSON lui est légèrement différent mais suit la même logique en terme de structure. Les deux combinés structurent une grande partie des données accessibles sur le web.</p>

## Les liens du jour

+ [JSON pour les débutants](https://la-cascade.io/articles/json-pour-les-debutants)
+ [https://www.courses.tegabrain.com/cc18/json-basics/](https://www.courses.tegabrain.com/cc18/json-basics/)

## Les objets javascript

Un objet JavaScript possède donc plusieurs propriétés qui lui sont associées. Une propriété peut être vue comme une variable attachée à l'objet. Les propriétés d'un objet sont des variables tout ce qu'il y a de plus classiques, exception faite qu'elles sont attachées à des objets. Les propriétés d'un objet représentent ses caractéristiques et on peut y accéder avec une notation utilisant le point « . », de la façon suivante. [Source](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Working_with_Objects)

```javascript
nomObjet.nomPropriete
```

On peut également imbriquer des objets.

```javascript
// objet JS; non valide en JSON
let myAnimal = {
  species: "dog",
  breed: "Labrador Retriever",
  age: 6,
  traits: {
    eyeColor: "brown",
    coatColor: "yellow",
    weight: "137lbs"
  }
}
```

## Le format JSON

Un exemple de syntaxe JSON. Chaque propriété est écrite sous forme de chaine de caractère. Le fichier JSON ne peut contenir de fonction (c'est ce qui fait son universalité). C'est un format d'échange de données qui n'est pas spécifique à javascript. Aujourd'hui ce format est utilisé avec d'autres langages également.

```javascript
{
  "espèce": "Dog",
  "race": "Labrador Retriever",
  "couleur": "Yellow",
  "âge": 6
}
```

## Exercices

+ Créer une page web à partir d'un fichier JSON hébergé dans notre projet. Vous pouvez aller chercher un fichier qui vous convient sur le dépôt suivant: [https://github.com/dariusk/corpora](https://github.com/dariusk/corpora)
+ Afficher la liste des articles (juste les titres) du site digitalab.be
+ Trouver un site wordpress sur le web et en faire un page d'accueil
+ Créer une horloge HTML
## Datasets & APIs
### Tests

+ [https://jsonplaceholder.typicode.com/posts](https://jsonplaceholder.typicode.com/posts)
+ [https://reqres.in/](https://reqres.in/)
+ ...
### Autres

+ [http://api.open-notify.org/astros.json](http://api.open-notify.org/astros.json)
+ [Wordpress API](http://www.digitalab.be/wp-json/wp/v2/posts)
+ [APIs publiques](https://github.com/public-apis/public-apis)
+ [https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson](https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson)
+ [https://github.com/Einenlum/french-verbs-list](https://github.com/Einenlum/french-verbs-list)
+ [https://opendata.stib-mivb.be/store/data](https://opendata.stib-mivb.be/store/data)
+ [https://www.infotec.be/fr-be/minformer/opendata.aspx](https://www.infotec.be/fr-be/minformer/opendata.aspx)
+ [Data driven documents](https://d3js.org/)
+ [https://www.zooniverse.org/projects/zooniverse/floating-forests](https://www.zooniverse.org/projects/zooniverse/floating-forests)
+ [Open Data Barometer](https://opendatabarometer.org/4thedition/detail-country/?_year=2016&indicator=ODB&detail=BEL)

