---
title: La vidéo
lang: fr-FR
---

## Jouer une vidéo

```javascript 
var maVideo;
function setup() {
    maVideo=createVideo("output.mp4");
    maVideo.loop();
}
function draw() {
}
```

### Changer sa teinte en fonction de la position de la souris

```javascript    
var maVideo;

function setup() {
	createCanvas(800,600);
	maVideo=createVideo("output.mp4");
	maVideo.loop();
	maVideo.hide();
}

function draw() {
  background(255);
  image(maVideo, 0, 0, width, height);
  tint(map(mouseX,0,windowWidth,0,360),100,100,255) // teinter l'image précédement dessinée en fonction de la position de la souris
}

```     

## Lancer la webcam

```javascript      
var capture;

function setup() {
  createCanvas(480, 480);
  capture = createCapture(VIDEO);
  capture.hide();
}

function draw() {
  image(capture, 0, 0, width, width * capture.height / capture.width);
  filter(INVERT);
}
```  