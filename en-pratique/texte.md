---
title: Travailler avec du texte
lang: fr-FR
---

# Travailler avec du texte

Cette page reprend la base de la fonction text() (et fonctions annexes), ainsi qu'une série de ressources et tutoriels plus avancés, mais pouvant être mis en oeuvre assez rapidement.
En effet, à l'instar de ce qui se fait pour le traitement de la video, l'export PDF ou la création d'interfaces utilisateur, une série de bibliothèques (libraries) proposent un grand nombre de fonctions avancées pour la manipulation du texte.

<a data-fancybox title="" href="/textexemple.png">![](/textexemple.png)</a>

Une entrée en matière : [https://creative-coding.decontextualize.com/text-and-type/](https://creative-coding.decontextualize.com/text-and-type/)

La façon la plus simple d'afficher du texte dans p5js est la suivante:

```js
function setup() {
    createCanvas(300,200);
    fill(200,0,0);//Plutôt rouge
    textSize(24);
    text("Un texte", 10, 30);
}
```

Ce chapitre va vous permettre de personnaliser l'usage des textes dans Processing en utilisant des polices de caractères alternatives.

## Utiliser une police de caractères

Les typos peuvent être chargées de différentes manières. Typos systèmes, typos en lignes & typos installées sur votre pc sont utilisables…

### Typos systèmes

Ce sont les typographies communément installées sur tout les ordinateurs. La police choisie doit être installée sur la machine qui éxecute le sketch.

```js
function setup() {
    createCanvas(800, 600);
    textFont("Arial");
    textSize(102);
}
function draw() {
    text("Hello world", 50, 120);
}
```

### Typo locale

Pour les fonts installées en local la méthode est similaire au chargement d’images.
p5.js supporte les fonts ttf & les fonts otf

1. Copier le fichier de la typo utilisée dans le dossier de votre animation.
2. Créer une variable.
3. Dans la fonction preload charger le fichier de la typo.

```js
var robotoCondensed;

function preload(){
    robotoCondensed = loadFont('assets/RobotoCondensed-Regular.ttf');
}

function setup() {
    createCanvas(800, 600);
    textFont(robotoCondensed);
    textSize(60);
}
function draw() {
    text("Il est aussi possible d'intégrer le texte dans un boîte.", 50, 50, 400, 400);
}
```

## Typographie: Pour aller plus loin

- [https://b2renger.github.io/p5js_typo/](https://b2renger.github.io/p5js_typo/)