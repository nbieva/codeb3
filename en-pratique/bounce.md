---
title: Boum boum
lang: fr-FR
---

# Les conditions en action

## Faire rebondir une ellipse sur les bords du sketch

Faire rebondir une ellipse sur les bords est une question qui revient souvent (qu'il s'agisse d'ellipses ou d'autres formes). Cette page reprend une façon de faire (il y en a d'autres) adaptée à ce que nous avons vu aujourd'hui. Elle est une façon de mettre en pratique les conditions.

Vous trouverez donc ci-dessous un code relativement simple, et commenté, que vous pouvez copier/modifier/corriger, etc. Nous vous conseillons vraiment de prendre le temps de lire avec attention les commentaires dans le code. Ils sont là pour vous expliquer ce qui s'y passe..

<iframe class="p5embed" height="320" src="https://editor.p5js.org/WorkshopsB1/full/qJErf-uTU"></iframe>

```javascript
// On crée une variable "vitesseH" (pour la vitesse du déplacement horizontal) et on lui assigne la valeur 10
var vitesseH = 10; 
// On crée une variable "vitesseV" (pour la vitesse du déplacement vertical) et on lui assigne la valeur 10
var vitesseV = 10; 
// On crée des variables pour les positions en X et Y de notre objet (ellipse). 
var posx;
var posy;
// On crée une variable "dia" et pour le diamètre de l'ellipse
var dia = 60; 

function setup() {
    createCanvas(800, 400);    
    // On défini une position de départ aléatoire sur l'axe des X (horizontalement), entre 0 et la largeur.
    posx = random(0, width);
    // On commence toujours en heut, donc..
    posy = 0;
}

function draw() {
  background(0, 20, 80);
    ellipse(posx, posy, dia, dia);
    // On augmente, à chaque boucle draw, la valeur de posx de la valeur de "vitesseH" 
    posx = posx+vitesseH;
    // On augmente, à chaque boucle draw, la valeur de posy de la valeur de "vitesseV"
    posy = posy+vitesseV;
  
    // Si posx est plus grand que la largeur, ou (||) si posx est plus petit que zéro, alors on multiplie posx par -1, de façon à inverser le sens. (Moins par moins donne plus)
    if(posx > width || posx < 0) {
        vitesseH = -vitesseH;
    }
  // Si posy est plus grand que la hauteur, ou (||) si posy est plus petit que zéro, alors on multiplie posy par -1 également.
  if(posy > height || posy < 0) {
        vitesseV = -vitesseV;
    }
}
```

## Moyens d'aller plus loin..

+ Que se passe-t-il si l'opacité de votre ellipse est de 50%, par exemple?
+ Que se passe-t-il si le background n'est pas réexécuté au début de chaque boucle mais uniquement en début de programme (setup)?
+ Que se passe-t-il si vous faites varier vos vitesses en X et Y indépendamment l'une de l'autre? Exploitez.
+ Et comment faites-vous pour faire varier ces vitesses? EN fonction de quoi?
+ etc.

Maintenant que les moments de rebonds sont identifiés, vous pouvez y ajouter d'autres événements (jouer un son, changer de couleur, etc.)