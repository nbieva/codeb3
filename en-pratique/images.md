---
title: Travailler avec des images
lang: fr-FR
---

# Travailler avec des images

## Importer une image

Les images que vous utilisez doivent se trouver dans le dossier de votre sketch (ici dans un dossier "assets"). Pour afficher les fichiers de votre sketch, cliquez sur la flèche (>) située en dessous du bouton start (▸).

<Image file="fichiers.png" imgsmall/>

Cliquez sur ▾ à droite de *Sketch Files* puis **Create folder**, appelez votre dossier *assets*.

<Image file="createfolder.png" imgsmall/>

Ensuite, cliquez sur ▾ à droite de *assets* puis **Upload file**. Uplloadez votre image.

<Image file="uploadfile.png" imgsmall/>

Votre dossier doit maintenant ressembler à ceci:

<Image file="fichiers2.png" imgsmall/>

```javascript
var img; // déclaration de la variable image

function setup() {
    createCanvas(600, 600);    
    img = loadImage("assets/exemple.jpg"); // attention, pas de caractères spéciaux ni accents ni espaces
}

function draw() {
    image(img, 0, 0); // on affiche l'image en haut à gauche de la page.
}
```

**Note:** Essayez maintenant de faire en sorte que l'image suive votre souris

<Image file="imagemouse.png" imgsmall/>

## Redimensionner une image

```javascript
var img; // déclaration de la variable image

function setup() {
  createCanvas(600, 600);
  background(0);
  img = loadImage("assets/exemple.jpg"); // attention, pas de caractères spéciaux ni accents ni espaces
}

function draw() {
  image(img, 0, 0, width / 2, height / 2);
}
```

<Image file="redimimg.png" imgsmall />

## Changer la teinte de l'image

```javascript
var img; // déclaration de la variable image

function setup() {
  createCanvas(600, 600);
  background(0);
  img = loadImage("assets/exemple.jpg"); // attention, pas de caractères spéciaux ni accents ni espaces
}

function draw() {
  tint(255, 0, 0, 128); // teinte en rouge et opacité à 50%
  image(img, 0, 0, width, height);
}
```

<Image file="image-rouge.png" imgsmall />

### Les différents types d'images

- JPG : le format le plus commun, idéal pour les photos et les visuels très riches en couleurs
- PNG-8 : le format recommandé pour les graphiques simples, les icônes et les logos
- GIF : pour toutes les images animées qui ponctuent vos messages
- PNG-24 : idéal si vous cherchez un effet de transparence ou pour les images complexes
- SVG : un format recommandé pour les images vectorisées, vous pouvez ainsi en changer le format sans perdre en qualité

----

### En savoir plus :

- [https://p5js.org/reference/#/p5/image](https://p5js.org/reference/#/p5/image)