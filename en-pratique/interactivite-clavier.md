---
title: Interactivité clavier
lang: fr-FR
---

# Interactivité clavier

<Image file="clavier.png" link="https://keycode.info" textlink="keycode.info"/>

## Capter l'événement clavier (n'importe quelle touche)

La fonction à utiliser est la fonction **keyPressed()**. Vous pouvez utiliser ceci pour changer une couleur, ou exporter une image. Dans l'exemple ci-dessous, la fonction est une fonction de premier niveau (à insérer au même niveau que **draw()** ou **setup()** )

```javascript
var value = 0;

function setup() {
  createCanvas(400, 400);  
  background(value);
}

function draw() {
}

function keyPressed() {
  if (value == 0) {
    value = 255;
  } else {
    value = 0;
  }
  background(value);
}

```

## Cibler une ou plusieurs touches en particulier (lettres)

Dans l'exemple suivant, on utilise une condition, nécessairement à l'intérieur de la boucle **draw()**. On utilise même deux conditions imbriquées puisque l'on vérifie d'abord si une touche du clavier est pressée, avant de vérifier s'il s'agit de la lettre B (en majuscule ou minuscule..)

```javascript
function setup() {
  createCanvas(400, 400);  
  background(0);
}

function draw() {
  if (keyIsPressed) {
    if (key == 'b' || key == 'B') {
      fill(0);
    }
  } else {
    fill(255);
  }
  rect(25, 25, 100, 100);
}
```

## Cibler d'autres touches du clavier

Vous pouvez également cibler d'autres touches du clavier (comme ESPACE, ENTER, FLECHES, etc.). A chaque touche de votre clavier correspond un code. Utilisez ce site pour connaître le code d'une touche de votre clavier: [http://keycode.info/](http://keycode.info/).

```js
var fillVal = 255;

function setup() {
  createCanvas(400, 400);  
  background(0);
}

function draw() {
  fill(fillVal);
  rect(25, 25, width/2, height/2);
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    fillVal = 255;
  } else if (keyCode === DOWN_ARROW) {
    fillVal = 0;
  }
  return false; // prevent default
}
```
