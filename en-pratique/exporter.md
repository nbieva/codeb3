---
title: Exporter
lang: fr-FR
sidebarDepth: 0
---

# Exporter

## Exporter votre canvas

Dans P5js, un peu comme dans Processing, vous pouvez exporter le canvas sous forme de fichier JPG ou PNG par exemple. Le tout est de savoir formuler à quel moment (quand on presse une touche? ou toutes les 20 secondes? ou à 16 heures précises? etc..) et de générer des noms de fichiers différents pour éviter qu'ils ne s'écrasent les uns les autres.

+ [https://p5js.org/reference/#/p5/save](https://p5js.org/reference/#/p5/save)

On utilise parfois le code ci-dessous pour exporter une image lorsque je presse la touche "s" (à ne pas mettre dans le draw!!):

```javascript
// Cette fonction doit se mettre au même niveau que le setup ou le draw
function keyTyped() {
  if (key === 's') {
    save("doeJohn-"+hour()+"-"+minute()+"-"+second()+".jpg");
  } 
}
```
Cette façon de faire ne fonctionnera que si la condition peut être vérifiée et qu'une boucle est donc active.

Nous intégrons également quelques éléments variables au nom du fichier afin de ne pas écraser les précédents. Evitez les fonctions du type **save("nomPrenom-option.png");**

Ce qui devrait au final donner une chaîne de caractère du type **doeJohn-10-22-56.jpg** et a toutes les chances d'être différent de vos autres fichiers.

A priori, tous vos fichiers sauvés vont se retrouver dans votre dossier **Téléchargements**.

## Export SVG

Solution pratique et efficace pour exporter votre sketch au format SVG. Ressources originales (merci à eux!) :

Copiez le code ci-dessous dans l'entête du fichier HTML de votre sketch (faites appel à nous pour ceci si nécessaire):

```javascript
// Cette fonction doit se mettre au même niveau que le setup ou le draw
<script src="https://unpkg.com/p5.js-svg@1.3.1"></script>
```
Ajoutez ensuite le paramètre "SVG" à votre fonction createCanvas(), comme ceci :

```javascript
function setup() {
    createCanvas(100, 100, SVG);
}
```
Sauvez ensuite votre SVG, comme vous le feriez pour un jpg, mais avec l'extension svg. Placez cette fonction hors de toute boucle dans votre code bien évidemment..

```javascript
save("monfichier.svg");
```

+ Article très complet de Gorilla Sun: [https://gorillasun.de/blog/working-with-svgs-in-p5js](https://gorillasun.de/blog/working-with-svgs-in-p5js)
+ Zenozeng's SVG runtime for P5:[https://github.com/zenozeng/p5.js-svg](https://github.com/zenozeng/p5.js-svg)
+ [http://zenozeng.github.io/p5.js-svg/examples/#exporting](http://zenozeng.github.io/p5.js-svg/examples/#exporting)

<iframe class="video" width="640" height="425" src="https://www.youtube.com/embed/_pZQxnkfNMA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<!-- ## Export au format PDF (vectoriel) avec Processing

::: tip
L'export en PDF nécessite l'importation d'une bibliothèque (fonctions supplémentaires) dans votre sketch Processing. Une *bibliothèque* est un ensemble de fonctions utilitaires, regroupées et mises à disposition afin de pouvoir être utilisées sans avoir à les réécrire.
:::

+ Ouvrez votre sketch
+ Cliquez sur **Sketch** > **Importer une librairie** > **Ajouter une librairie** (Si vous ne voyez pas PDF Export dans la liste)
+ Cherchez et choisissez **PDF Export** (processing foundation), puis cliquez sur **Install**. Fermer ensuite la boîte de dialogue.
+ Importez la librairie dans votre Sketch ( **Sketch** > **Importer une librairie** > Choix dans la liste )

Le PDF étant un format vectoriel, il aura l'avantage de pouvoir être exploité et retravaillé directement dans un logiciel de dessin vectoriel tels Illustrator ou Inkscape.

Les différentes façons d'enregistrer vos images sont détaillées, très clairement ici:

[https://processing.org:8443/reference/libraries/pdf/index.html](https://processing.org:8443/reference/libraries/pdf/index.html)

[https://processing.org/reference/libraries/pdf/](https://processing.org/reference/libraries/pdf/)

Voir aussi la section INPUT dans la [référence Processing](https://processing.org/reference/)

<a data-fancybox title="" href="/pdfcap.png">![](/pdfcap.png)</a>

```java
import processing.pdf.*;

int x1;
int y1;
int x2;
int y2;
int diametre = 10;

void setup() {
    size(800,600);
    beginRecord(PDF, "lignes.pdf");
    strokeWeight(0.5);
}

void draw() {
    fill(random(255),random(255),random(255));
    x1 = round(random(15,width-15));
    y1 = round(random(15,height-15));
    x2 = round(random(15,width-15));
    y2 = round(random(15,height-15));

    stroke(0,180);
    line(x1,y1,x2,y2);
    
    noStroke();
    ellipse(x1,y1,diametre,diametre);
    ellipse(x2,y2,diametre,diametre);
}
void keyPressed() {
    endRecord();
    stop();
}
```

Dans le code ci-dessus, les fonctions importantes concernant l'enregistrement de votre PDF sont les suivantes:
```java
    import processing.pdf.*; 
    beginRecord(PDF, "lignes.pdf"); 
    endRecord(); 
```
 -->

---

Le fichier généré pourrait très bien être ensuite exploité dans Illustrator ou Inkscape...

<a data-fancybox title="" href="/capdf1.png">![](/capdf1.png)</a>

<a data-fancybox title="" href="/capdf2.png">![](/capdf2.png)</a>

<a data-fancybox title="" href="/capdf3.png">![](/capdf3.png)</a>