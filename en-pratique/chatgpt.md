---
title: IA / ChatGPT
lang: fr-FR
---

# IA: ChatGPT

<Image file="chatgpt0.jpg" legend="Chat GPT" />

<iframe class="p5embed" width="740" height="400" src="https://editor.p5js.org/WorkshopsB1/full/c3k1agzTV"></iframe>



Générer du code est quelque chose que ChatGPT fait plutôt bien, et ce dans beaucoup de langages différents. En effet, le web regorge de discussions/questions/réponses à une multitudes de problèmes dans ce domaine. Une mine d'or donc, pour ces modèles qui s'en nourrissent.. 

[https://chat.openai.com/](https://chat.openai.com/)

P5js (javascript) ne fait pas excxeption à la règle et ChatGPT pourra probablement vous aider pour autant que vous puissiez:

+ **Formuler** correctement votre demande.
+ **Corriger** ses éventuelles erreurs ou **compléter** le code qu'il vous donne.

En effet, parfois ChatGPT ne vous donnera qu'une partie d'un script et non pas le script en entier, et dans certains cas (si vous voulez importer une image) il vous donnera des exemples (ChatGPT ne connait pas le nom de votre image) que vous devrez modifier/compléter. Vous pouvez également lui demander de se corriger lui-même. parfois cela suffit.

Mais il est donc très utile de savoir lire un minimum le code qu'il vous renvoie. 

Dans notre cas, n'oubliez pas bien évidemment de mentionner P5js dans votre demande (prompt), histoire qu'il ne vous renvoie pas un script Python (ce qui est aussi intéressant mais c'est encore une autre aventure..).

Notez aussi que ChatGPT commente son code et vous l'explique bien souvent (voir ci-dessous).

## Un petit exemple sans filet...

### Notre demande: 
"avec P5js, je voudrais créer un script qui simule une pluie, dans un format de 800x600"

### Réponse de Chat GPT:
*Vous pouvez créer une simulation de pluie en utilisant P5.js avec un canevas de 800x600 pixels. Voici un exemple de script qui simule une pluie :*

```js{3,4,8,10,11,16,39,40} 
let drops = [];

function setup() {
  createCanvas(800, 600);
  for (let i = 0; i < 100; i++) {
    drops[i] = new Drop();
  }
}

function draw() {
  background(0);
  for (let i = 0; i < drops.length; i++) {
    drops[i].fall();
    drops[i].show();
  }
}

class Drop {
  constructor() {
    this.x = random(width);
    this.y = random(-500, -50);
    this.z = random(0, 20);
    this.len = map(this.z, 0, 20, 10, 20);
    this.yspeed = map(this.z, 0, 20, 1, 20);
  }

  fall() {
    this.y = this.y + this.yspeed;
    let grav = map(this.z, 0, 20, 0, 0.2);
    this.yspeed = this.yspeed + grav;
    if (this.y > height) {
      this.y = random(-200, -100);
      this.yspeed = map(this.z, 0, 20, 4, 10);
    }
  }

  show() {
    let thick = map(this.z, 0, 20, 1, 3);
    stroke(138, 43, 226); // Couleur violette
    line(this.x, this.y, this.x, this.y + this.len);
  }
}


```     

## Commentaire de ChatGPT:

*Ce script crée un certain nombre de gouttes de pluie qui tombent de manière aléatoire sur le canevas. Chaque goutte a une longueur, une vitesse de descente et une épaisseur différentes, ce qui donne un effet de pluie réaliste. La couleur des gouttes est définie en violet, mais vous pouvez la personnaliser à votre convenance.*

*Assurez-vous d'inclure la bibliothèque P5.js dans votre projet pour que ce code fonctionne correctement. Vous pouvez également ajuster les paramètres pour obtenir l'effet de pluie souhaité.*

## Compréhension à la lecture: 15% :-|

Ce code comprend des éléments et concepts que nous n'borderons pas dans les workshops (on ne sait pas tout faire en un jour..), comme les tableaux ou les objets, mais nous pouvons déjà identifier et reconnaitre certains éléments (lignes surlignées) que nous pouvons exploiter et customiser. 

Si nous voulons faire varier l'épaisseur des lignes, par exemple, nous pouvons donc évaluer l'emplacement le plus adéquat où placer notre fonction [strokeWeight()](https://p5js.org/reference/#capdf1strokeWeight).

