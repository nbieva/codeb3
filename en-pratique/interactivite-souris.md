---
title: Interactivité souris
lang: fr-FR
---

# Interactivité souris

## Avec la souris

<a data-fancybox title="Pointer, pointer" href="/mouse.png">![Pointer, pointer](/mouse.png)</a>
Jonathan Puckey, [*Pointer, pointer*](https://pointerpointer.com/)

Pointer Pointer is an experiment in interaction, a celebration of the disappearing mouse pointer and a rumination on Big Data.
(+ https://www.youtube.com/watch?v=Z2ZXW2HBLPM )

## Un outil de dessin

```js
function setup() {
    // dimension du canvas
    createCanvas(800,600);
    // fond blanc
    background(255);
}

function draw(){
    // on dessine une ellipse dont les positions x et y correspondent à la position de la souris.
    ellipse(mouseX, mouseY, 20, 20);
}

```

## Changer de couleur au clic

```js
function setup() {
    createCanvas(600, 600);
    background(255);
}

function draw(){
}

// fonction détectant si l'on clique sur la souris
function mouseClicked() {
    background(random(255), random(255), random(255)); // on change la couleur du fond aléatoirement
}

```

## Déplacer des éléments

```js
var bx = 0; // largeur de ma box
var by = 0; // hauteur de ma box
var boxSize = 75; // taille de ma box
var overBox = false; // variable pour définir si la souris survol la box
var locked = false; // variable pour définir si l'on a selectionné la box
// variables utilisées pour définir la position du carré
var xOffset = 0.0;
var yOffset = 0.0;

function setup() {
    createCanvas(600, 600);
    // on défini que le rectangle se dessinera à partir du centre
    rectMode(RADIUS);
}

function draw() {
    background(0);
    // Test si le cursor est sur la box
    if (mouseX > bx-boxSize && mouseX < bx+boxSize &&
        mouseY > by-boxSize && mouseY < by+boxSize) {
        overBox = true; // on change la variable pour indiquer que la box est survolée
    } else {
        overBox = false;
    }
    // Draw the box
    stroke(153);
    fill(153);
    rect(bx, by, boxSize, boxSize);
}

// fonction détectant si l'on clique sur la souris
function mousePressed() {
    if(overBox == true) { // on vérifie l'état de la variable
        locked = true; // indique que l'utilisateur a cliqué
    } else {
        locked = false;
    }
    // on défini la position du rectangle en fonction de la souris
    xOffset = mouseX-bx;
    yOffset = mouseY-by;
}

// fonction pour détecter le déplacement de la souris si elle est cliquée
function mouseDragged() {
    if(locked) { // vérifier que l'utilisateur a cliqué
        // mise à jour des variables déterminant la position du rectangle
        bx = mouseX-xOffset;
        by = mouseY-yOffset;
    }
}

// fonction pour détecter que l'utilisateur à relaché le clique
function mouseReleased() {
    locked = false;
}
```